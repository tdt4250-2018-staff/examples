/**
 * generated by Xtext 2.14.0
 */
package no.hal.quiz.ide;

import no.hal.quiz.ide.AbstractXquizIdeModule;

/**
 * Use this class to register ide components.
 */
@SuppressWarnings("all")
public class XquizIdeModule extends AbstractXquizIdeModule {
}
