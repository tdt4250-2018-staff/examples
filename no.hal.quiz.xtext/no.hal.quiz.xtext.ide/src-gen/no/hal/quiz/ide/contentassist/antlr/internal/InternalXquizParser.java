package no.hal.quiz.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import no.hal.quiz.services.XquizGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalXquizParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'no'", "'quiz'", "'.'", "'part'", "'@'", "'?'", "'('", "')'", "'['", "']'", "'~'", "'yes'", "'x'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=5;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalXquizParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalXquizParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalXquizParser.tokenNames; }
    public String getGrammarFileName() { return "InternalXquiz.g"; }


    	private XquizGrammarAccess grammarAccess;

    	public void setGrammarAccess(XquizGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleQuiz"
    // InternalXquiz.g:53:1: entryRuleQuiz : ruleQuiz EOF ;
    public final void entryRuleQuiz() throws RecognitionException {
        try {
            // InternalXquiz.g:54:1: ( ruleQuiz EOF )
            // InternalXquiz.g:55:1: ruleQuiz EOF
            {
             before(grammarAccess.getQuizRule()); 
            pushFollow(FOLLOW_1);
            ruleQuiz();

            state._fsp--;

             after(grammarAccess.getQuizRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQuiz"


    // $ANTLR start "ruleQuiz"
    // InternalXquiz.g:62:1: ruleQuiz : ( ( rule__Quiz__Group__0 ) ) ;
    public final void ruleQuiz() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:66:2: ( ( ( rule__Quiz__Group__0 ) ) )
            // InternalXquiz.g:67:2: ( ( rule__Quiz__Group__0 ) )
            {
            // InternalXquiz.g:67:2: ( ( rule__Quiz__Group__0 ) )
            // InternalXquiz.g:68:3: ( rule__Quiz__Group__0 )
            {
             before(grammarAccess.getQuizAccess().getGroup()); 
            // InternalXquiz.g:69:3: ( rule__Quiz__Group__0 )
            // InternalXquiz.g:69:4: rule__Quiz__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Quiz__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQuizAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQuiz"


    // $ANTLR start "entryRuleQName"
    // InternalXquiz.g:78:1: entryRuleQName : ruleQName EOF ;
    public final void entryRuleQName() throws RecognitionException {
        try {
            // InternalXquiz.g:79:1: ( ruleQName EOF )
            // InternalXquiz.g:80:1: ruleQName EOF
            {
             before(grammarAccess.getQNameRule()); 
            pushFollow(FOLLOW_1);
            ruleQName();

            state._fsp--;

             after(grammarAccess.getQNameRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQName"


    // $ANTLR start "ruleQName"
    // InternalXquiz.g:87:1: ruleQName : ( ( rule__QName__Group__0 ) ) ;
    public final void ruleQName() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:91:2: ( ( ( rule__QName__Group__0 ) ) )
            // InternalXquiz.g:92:2: ( ( rule__QName__Group__0 ) )
            {
            // InternalXquiz.g:92:2: ( ( rule__QName__Group__0 ) )
            // InternalXquiz.g:93:3: ( rule__QName__Group__0 )
            {
             before(grammarAccess.getQNameAccess().getGroup()); 
            // InternalXquiz.g:94:3: ( rule__QName__Group__0 )
            // InternalXquiz.g:94:4: rule__QName__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QName__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQNameAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQName"


    // $ANTLR start "entryRuleAbstractQuizPart"
    // InternalXquiz.g:103:1: entryRuleAbstractQuizPart : ruleAbstractQuizPart EOF ;
    public final void entryRuleAbstractQuizPart() throws RecognitionException {
        try {
            // InternalXquiz.g:104:1: ( ruleAbstractQuizPart EOF )
            // InternalXquiz.g:105:1: ruleAbstractQuizPart EOF
            {
             before(grammarAccess.getAbstractQuizPartRule()); 
            pushFollow(FOLLOW_1);
            ruleAbstractQuizPart();

            state._fsp--;

             after(grammarAccess.getAbstractQuizPartRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAbstractQuizPart"


    // $ANTLR start "ruleAbstractQuizPart"
    // InternalXquiz.g:112:1: ruleAbstractQuizPart : ( ( rule__AbstractQuizPart__Alternatives ) ) ;
    public final void ruleAbstractQuizPart() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:116:2: ( ( ( rule__AbstractQuizPart__Alternatives ) ) )
            // InternalXquiz.g:117:2: ( ( rule__AbstractQuizPart__Alternatives ) )
            {
            // InternalXquiz.g:117:2: ( ( rule__AbstractQuizPart__Alternatives ) )
            // InternalXquiz.g:118:3: ( rule__AbstractQuizPart__Alternatives )
            {
             before(grammarAccess.getAbstractQuizPartAccess().getAlternatives()); 
            // InternalXquiz.g:119:3: ( rule__AbstractQuizPart__Alternatives )
            // InternalXquiz.g:119:4: rule__AbstractQuizPart__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__AbstractQuizPart__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getAbstractQuizPartAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAbstractQuizPart"


    // $ANTLR start "entryRuleQuizPartRef"
    // InternalXquiz.g:128:1: entryRuleQuizPartRef : ruleQuizPartRef EOF ;
    public final void entryRuleQuizPartRef() throws RecognitionException {
        try {
            // InternalXquiz.g:129:1: ( ruleQuizPartRef EOF )
            // InternalXquiz.g:130:1: ruleQuizPartRef EOF
            {
             before(grammarAccess.getQuizPartRefRule()); 
            pushFollow(FOLLOW_1);
            ruleQuizPartRef();

            state._fsp--;

             after(grammarAccess.getQuizPartRefRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQuizPartRef"


    // $ANTLR start "ruleQuizPartRef"
    // InternalXquiz.g:137:1: ruleQuizPartRef : ( ( rule__QuizPartRef__Group__0 ) ) ;
    public final void ruleQuizPartRef() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:141:2: ( ( ( rule__QuizPartRef__Group__0 ) ) )
            // InternalXquiz.g:142:2: ( ( rule__QuizPartRef__Group__0 ) )
            {
            // InternalXquiz.g:142:2: ( ( rule__QuizPartRef__Group__0 ) )
            // InternalXquiz.g:143:3: ( rule__QuizPartRef__Group__0 )
            {
             before(grammarAccess.getQuizPartRefAccess().getGroup()); 
            // InternalXquiz.g:144:3: ( rule__QuizPartRef__Group__0 )
            // InternalXquiz.g:144:4: rule__QuizPartRef__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QuizPartRef__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQuizPartRefAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQuizPartRef"


    // $ANTLR start "entryRuleQuizPart"
    // InternalXquiz.g:153:1: entryRuleQuizPart : ruleQuizPart EOF ;
    public final void entryRuleQuizPart() throws RecognitionException {
        try {
            // InternalXquiz.g:154:1: ( ruleQuizPart EOF )
            // InternalXquiz.g:155:1: ruleQuizPart EOF
            {
             before(grammarAccess.getQuizPartRule()); 
            pushFollow(FOLLOW_1);
            ruleQuizPart();

            state._fsp--;

             after(grammarAccess.getQuizPartRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQuizPart"


    // $ANTLR start "ruleQuizPart"
    // InternalXquiz.g:162:1: ruleQuizPart : ( ( rule__QuizPart__Group__0 ) ) ;
    public final void ruleQuizPart() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:166:2: ( ( ( rule__QuizPart__Group__0 ) ) )
            // InternalXquiz.g:167:2: ( ( rule__QuizPart__Group__0 ) )
            {
            // InternalXquiz.g:167:2: ( ( rule__QuizPart__Group__0 ) )
            // InternalXquiz.g:168:3: ( rule__QuizPart__Group__0 )
            {
             before(grammarAccess.getQuizPartAccess().getGroup()); 
            // InternalXquiz.g:169:3: ( rule__QuizPart__Group__0 )
            // InternalXquiz.g:169:4: rule__QuizPart__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QuizPart__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQuizPartAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQuizPart"


    // $ANTLR start "entryRuleAbstractQA"
    // InternalXquiz.g:178:1: entryRuleAbstractQA : ruleAbstractQA EOF ;
    public final void entryRuleAbstractQA() throws RecognitionException {
        try {
            // InternalXquiz.g:179:1: ( ruleAbstractQA EOF )
            // InternalXquiz.g:180:1: ruleAbstractQA EOF
            {
             before(grammarAccess.getAbstractQARule()); 
            pushFollow(FOLLOW_1);
            ruleAbstractQA();

            state._fsp--;

             after(grammarAccess.getAbstractQARule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAbstractQA"


    // $ANTLR start "ruleAbstractQA"
    // InternalXquiz.g:187:1: ruleAbstractQA : ( ( rule__AbstractQA__Alternatives ) ) ;
    public final void ruleAbstractQA() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:191:2: ( ( ( rule__AbstractQA__Alternatives ) ) )
            // InternalXquiz.g:192:2: ( ( rule__AbstractQA__Alternatives ) )
            {
            // InternalXquiz.g:192:2: ( ( rule__AbstractQA__Alternatives ) )
            // InternalXquiz.g:193:3: ( rule__AbstractQA__Alternatives )
            {
             before(grammarAccess.getAbstractQAAccess().getAlternatives()); 
            // InternalXquiz.g:194:3: ( rule__AbstractQA__Alternatives )
            // InternalXquiz.g:194:4: rule__AbstractQA__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__AbstractQA__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getAbstractQAAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAbstractQA"


    // $ANTLR start "entryRuleQARef"
    // InternalXquiz.g:203:1: entryRuleQARef : ruleQARef EOF ;
    public final void entryRuleQARef() throws RecognitionException {
        try {
            // InternalXquiz.g:204:1: ( ruleQARef EOF )
            // InternalXquiz.g:205:1: ruleQARef EOF
            {
             before(grammarAccess.getQARefRule()); 
            pushFollow(FOLLOW_1);
            ruleQARef();

            state._fsp--;

             after(grammarAccess.getQARefRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQARef"


    // $ANTLR start "ruleQARef"
    // InternalXquiz.g:212:1: ruleQARef : ( ( rule__QARef__Group__0 ) ) ;
    public final void ruleQARef() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:216:2: ( ( ( rule__QARef__Group__0 ) ) )
            // InternalXquiz.g:217:2: ( ( rule__QARef__Group__0 ) )
            {
            // InternalXquiz.g:217:2: ( ( rule__QARef__Group__0 ) )
            // InternalXquiz.g:218:3: ( rule__QARef__Group__0 )
            {
             before(grammarAccess.getQARefAccess().getGroup()); 
            // InternalXquiz.g:219:3: ( rule__QARef__Group__0 )
            // InternalXquiz.g:219:4: rule__QARef__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QARef__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQARefAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQARef"


    // $ANTLR start "entryRuleQA"
    // InternalXquiz.g:228:1: entryRuleQA : ruleQA EOF ;
    public final void entryRuleQA() throws RecognitionException {
        try {
            // InternalXquiz.g:229:1: ( ruleQA EOF )
            // InternalXquiz.g:230:1: ruleQA EOF
            {
             before(grammarAccess.getQARule()); 
            pushFollow(FOLLOW_1);
            ruleQA();

            state._fsp--;

             after(grammarAccess.getQARule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQA"


    // $ANTLR start "ruleQA"
    // InternalXquiz.g:237:1: ruleQA : ( ( rule__QA__Group__0 ) ) ;
    public final void ruleQA() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:241:2: ( ( ( rule__QA__Group__0 ) ) )
            // InternalXquiz.g:242:2: ( ( rule__QA__Group__0 ) )
            {
            // InternalXquiz.g:242:2: ( ( rule__QA__Group__0 ) )
            // InternalXquiz.g:243:3: ( rule__QA__Group__0 )
            {
             before(grammarAccess.getQAAccess().getGroup()); 
            // InternalXquiz.g:244:3: ( rule__QA__Group__0 )
            // InternalXquiz.g:244:4: rule__QA__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QA__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQAAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQA"


    // $ANTLR start "entryRuleQuestion"
    // InternalXquiz.g:253:1: entryRuleQuestion : ruleQuestion EOF ;
    public final void entryRuleQuestion() throws RecognitionException {
        try {
            // InternalXquiz.g:254:1: ( ruleQuestion EOF )
            // InternalXquiz.g:255:1: ruleQuestion EOF
            {
             before(grammarAccess.getQuestionRule()); 
            pushFollow(FOLLOW_1);
            ruleQuestion();

            state._fsp--;

             after(grammarAccess.getQuestionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQuestion"


    // $ANTLR start "ruleQuestion"
    // InternalXquiz.g:262:1: ruleQuestion : ( ruleStringQuestion ) ;
    public final void ruleQuestion() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:266:2: ( ( ruleStringQuestion ) )
            // InternalXquiz.g:267:2: ( ruleStringQuestion )
            {
            // InternalXquiz.g:267:2: ( ruleStringQuestion )
            // InternalXquiz.g:268:3: ruleStringQuestion
            {
             before(grammarAccess.getQuestionAccess().getStringQuestionParserRuleCall()); 
            pushFollow(FOLLOW_2);
            ruleStringQuestion();

            state._fsp--;

             after(grammarAccess.getQuestionAccess().getStringQuestionParserRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQuestion"


    // $ANTLR start "entryRuleStringQuestion"
    // InternalXquiz.g:278:1: entryRuleStringQuestion : ruleStringQuestion EOF ;
    public final void entryRuleStringQuestion() throws RecognitionException {
        try {
            // InternalXquiz.g:279:1: ( ruleStringQuestion EOF )
            // InternalXquiz.g:280:1: ruleStringQuestion EOF
            {
             before(grammarAccess.getStringQuestionRule()); 
            pushFollow(FOLLOW_1);
            ruleStringQuestion();

            state._fsp--;

             after(grammarAccess.getStringQuestionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStringQuestion"


    // $ANTLR start "ruleStringQuestion"
    // InternalXquiz.g:287:1: ruleStringQuestion : ( ( rule__StringQuestion__QuestionAssignment ) ) ;
    public final void ruleStringQuestion() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:291:2: ( ( ( rule__StringQuestion__QuestionAssignment ) ) )
            // InternalXquiz.g:292:2: ( ( rule__StringQuestion__QuestionAssignment ) )
            {
            // InternalXquiz.g:292:2: ( ( rule__StringQuestion__QuestionAssignment ) )
            // InternalXquiz.g:293:3: ( rule__StringQuestion__QuestionAssignment )
            {
             before(grammarAccess.getStringQuestionAccess().getQuestionAssignment()); 
            // InternalXquiz.g:294:3: ( rule__StringQuestion__QuestionAssignment )
            // InternalXquiz.g:294:4: rule__StringQuestion__QuestionAssignment
            {
            pushFollow(FOLLOW_2);
            rule__StringQuestion__QuestionAssignment();

            state._fsp--;


            }

             after(grammarAccess.getStringQuestionAccess().getQuestionAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStringQuestion"


    // $ANTLR start "entryRuleAnswer"
    // InternalXquiz.g:303:1: entryRuleAnswer : ruleAnswer EOF ;
    public final void entryRuleAnswer() throws RecognitionException {
        try {
            // InternalXquiz.g:304:1: ( ruleAnswer EOF )
            // InternalXquiz.g:305:1: ruleAnswer EOF
            {
             before(grammarAccess.getAnswerRule()); 
            pushFollow(FOLLOW_1);
            ruleAnswer();

            state._fsp--;

             after(grammarAccess.getAnswerRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAnswer"


    // $ANTLR start "ruleAnswer"
    // InternalXquiz.g:312:1: ruleAnswer : ( ( rule__Answer__Alternatives ) ) ;
    public final void ruleAnswer() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:316:2: ( ( ( rule__Answer__Alternatives ) ) )
            // InternalXquiz.g:317:2: ( ( rule__Answer__Alternatives ) )
            {
            // InternalXquiz.g:317:2: ( ( rule__Answer__Alternatives ) )
            // InternalXquiz.g:318:3: ( rule__Answer__Alternatives )
            {
             before(grammarAccess.getAnswerAccess().getAlternatives()); 
            // InternalXquiz.g:319:3: ( rule__Answer__Alternatives )
            // InternalXquiz.g:319:4: rule__Answer__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Answer__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getAnswerAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAnswer"


    // $ANTLR start "entryRuleOptionAnswer"
    // InternalXquiz.g:328:1: entryRuleOptionAnswer : ruleOptionAnswer EOF ;
    public final void entryRuleOptionAnswer() throws RecognitionException {
        try {
            // InternalXquiz.g:329:1: ( ruleOptionAnswer EOF )
            // InternalXquiz.g:330:1: ruleOptionAnswer EOF
            {
             before(grammarAccess.getOptionAnswerRule()); 
            pushFollow(FOLLOW_1);
            ruleOptionAnswer();

            state._fsp--;

             after(grammarAccess.getOptionAnswerRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOptionAnswer"


    // $ANTLR start "ruleOptionAnswer"
    // InternalXquiz.g:337:1: ruleOptionAnswer : ( ruleSimpleAnswer ) ;
    public final void ruleOptionAnswer() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:341:2: ( ( ruleSimpleAnswer ) )
            // InternalXquiz.g:342:2: ( ruleSimpleAnswer )
            {
            // InternalXquiz.g:342:2: ( ruleSimpleAnswer )
            // InternalXquiz.g:343:3: ruleSimpleAnswer
            {
             before(grammarAccess.getOptionAnswerAccess().getSimpleAnswerParserRuleCall()); 
            pushFollow(FOLLOW_2);
            ruleSimpleAnswer();

            state._fsp--;

             after(grammarAccess.getOptionAnswerAccess().getSimpleAnswerParserRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOptionAnswer"


    // $ANTLR start "entryRuleSimpleAnswer"
    // InternalXquiz.g:353:1: entryRuleSimpleAnswer : ruleSimpleAnswer EOF ;
    public final void entryRuleSimpleAnswer() throws RecognitionException {
        try {
            // InternalXquiz.g:354:1: ( ruleSimpleAnswer EOF )
            // InternalXquiz.g:355:1: ruleSimpleAnswer EOF
            {
             before(grammarAccess.getSimpleAnswerRule()); 
            pushFollow(FOLLOW_1);
            ruleSimpleAnswer();

            state._fsp--;

             after(grammarAccess.getSimpleAnswerRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSimpleAnswer"


    // $ANTLR start "ruleSimpleAnswer"
    // InternalXquiz.g:362:1: ruleSimpleAnswer : ( ( rule__SimpleAnswer__Alternatives ) ) ;
    public final void ruleSimpleAnswer() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:366:2: ( ( ( rule__SimpleAnswer__Alternatives ) ) )
            // InternalXquiz.g:367:2: ( ( rule__SimpleAnswer__Alternatives ) )
            {
            // InternalXquiz.g:367:2: ( ( rule__SimpleAnswer__Alternatives ) )
            // InternalXquiz.g:368:3: ( rule__SimpleAnswer__Alternatives )
            {
             before(grammarAccess.getSimpleAnswerAccess().getAlternatives()); 
            // InternalXquiz.g:369:3: ( rule__SimpleAnswer__Alternatives )
            // InternalXquiz.g:369:4: rule__SimpleAnswer__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__SimpleAnswer__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getSimpleAnswerAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSimpleAnswer"


    // $ANTLR start "entryRuleEDouble"
    // InternalXquiz.g:378:1: entryRuleEDouble : ruleEDouble EOF ;
    public final void entryRuleEDouble() throws RecognitionException {
        try {
            // InternalXquiz.g:379:1: ( ruleEDouble EOF )
            // InternalXquiz.g:380:1: ruleEDouble EOF
            {
             before(grammarAccess.getEDoubleRule()); 
            pushFollow(FOLLOW_1);
            ruleEDouble();

            state._fsp--;

             after(grammarAccess.getEDoubleRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEDouble"


    // $ANTLR start "ruleEDouble"
    // InternalXquiz.g:387:1: ruleEDouble : ( ( rule__EDouble__Group__0 ) ) ;
    public final void ruleEDouble() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:391:2: ( ( ( rule__EDouble__Group__0 ) ) )
            // InternalXquiz.g:392:2: ( ( rule__EDouble__Group__0 ) )
            {
            // InternalXquiz.g:392:2: ( ( rule__EDouble__Group__0 ) )
            // InternalXquiz.g:393:3: ( rule__EDouble__Group__0 )
            {
             before(grammarAccess.getEDoubleAccess().getGroup()); 
            // InternalXquiz.g:394:3: ( rule__EDouble__Group__0 )
            // InternalXquiz.g:394:4: rule__EDouble__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__EDouble__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEDoubleAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEDouble"


    // $ANTLR start "entryRuleStringAnswer"
    // InternalXquiz.g:403:1: entryRuleStringAnswer : ruleStringAnswer EOF ;
    public final void entryRuleStringAnswer() throws RecognitionException {
        try {
            // InternalXquiz.g:404:1: ( ruleStringAnswer EOF )
            // InternalXquiz.g:405:1: ruleStringAnswer EOF
            {
             before(grammarAccess.getStringAnswerRule()); 
            pushFollow(FOLLOW_1);
            ruleStringAnswer();

            state._fsp--;

             after(grammarAccess.getStringAnswerRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStringAnswer"


    // $ANTLR start "ruleStringAnswer"
    // InternalXquiz.g:412:1: ruleStringAnswer : ( ( rule__StringAnswer__Group__0 ) ) ;
    public final void ruleStringAnswer() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:416:2: ( ( ( rule__StringAnswer__Group__0 ) ) )
            // InternalXquiz.g:417:2: ( ( rule__StringAnswer__Group__0 ) )
            {
            // InternalXquiz.g:417:2: ( ( rule__StringAnswer__Group__0 ) )
            // InternalXquiz.g:418:3: ( rule__StringAnswer__Group__0 )
            {
             before(grammarAccess.getStringAnswerAccess().getGroup()); 
            // InternalXquiz.g:419:3: ( rule__StringAnswer__Group__0 )
            // InternalXquiz.g:419:4: rule__StringAnswer__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__StringAnswer__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getStringAnswerAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStringAnswer"


    // $ANTLR start "entryRuleNumberAnswer"
    // InternalXquiz.g:428:1: entryRuleNumberAnswer : ruleNumberAnswer EOF ;
    public final void entryRuleNumberAnswer() throws RecognitionException {
        try {
            // InternalXquiz.g:429:1: ( ruleNumberAnswer EOF )
            // InternalXquiz.g:430:1: ruleNumberAnswer EOF
            {
             before(grammarAccess.getNumberAnswerRule()); 
            pushFollow(FOLLOW_1);
            ruleNumberAnswer();

            state._fsp--;

             after(grammarAccess.getNumberAnswerRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNumberAnswer"


    // $ANTLR start "ruleNumberAnswer"
    // InternalXquiz.g:437:1: ruleNumberAnswer : ( ( rule__NumberAnswer__ValueAssignment ) ) ;
    public final void ruleNumberAnswer() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:441:2: ( ( ( rule__NumberAnswer__ValueAssignment ) ) )
            // InternalXquiz.g:442:2: ( ( rule__NumberAnswer__ValueAssignment ) )
            {
            // InternalXquiz.g:442:2: ( ( rule__NumberAnswer__ValueAssignment ) )
            // InternalXquiz.g:443:3: ( rule__NumberAnswer__ValueAssignment )
            {
             before(grammarAccess.getNumberAnswerAccess().getValueAssignment()); 
            // InternalXquiz.g:444:3: ( rule__NumberAnswer__ValueAssignment )
            // InternalXquiz.g:444:4: rule__NumberAnswer__ValueAssignment
            {
            pushFollow(FOLLOW_2);
            rule__NumberAnswer__ValueAssignment();

            state._fsp--;


            }

             after(grammarAccess.getNumberAnswerAccess().getValueAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNumberAnswer"


    // $ANTLR start "entryRuleBooleanAnswer"
    // InternalXquiz.g:453:1: entryRuleBooleanAnswer : ruleBooleanAnswer EOF ;
    public final void entryRuleBooleanAnswer() throws RecognitionException {
        try {
            // InternalXquiz.g:454:1: ( ruleBooleanAnswer EOF )
            // InternalXquiz.g:455:1: ruleBooleanAnswer EOF
            {
             before(grammarAccess.getBooleanAnswerRule()); 
            pushFollow(FOLLOW_1);
            ruleBooleanAnswer();

            state._fsp--;

             after(grammarAccess.getBooleanAnswerRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBooleanAnswer"


    // $ANTLR start "ruleBooleanAnswer"
    // InternalXquiz.g:462:1: ruleBooleanAnswer : ( ( rule__BooleanAnswer__Group__0 ) ) ;
    public final void ruleBooleanAnswer() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:466:2: ( ( ( rule__BooleanAnswer__Group__0 ) ) )
            // InternalXquiz.g:467:2: ( ( rule__BooleanAnswer__Group__0 ) )
            {
            // InternalXquiz.g:467:2: ( ( rule__BooleanAnswer__Group__0 ) )
            // InternalXquiz.g:468:3: ( rule__BooleanAnswer__Group__0 )
            {
             before(grammarAccess.getBooleanAnswerAccess().getGroup()); 
            // InternalXquiz.g:469:3: ( rule__BooleanAnswer__Group__0 )
            // InternalXquiz.g:469:4: rule__BooleanAnswer__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__BooleanAnswer__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBooleanAnswerAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBooleanAnswer"


    // $ANTLR start "entryRuleOptionsAnswer"
    // InternalXquiz.g:478:1: entryRuleOptionsAnswer : ruleOptionsAnswer EOF ;
    public final void entryRuleOptionsAnswer() throws RecognitionException {
        try {
            // InternalXquiz.g:479:1: ( ruleOptionsAnswer EOF )
            // InternalXquiz.g:480:1: ruleOptionsAnswer EOF
            {
             before(grammarAccess.getOptionsAnswerRule()); 
            pushFollow(FOLLOW_1);
            ruleOptionsAnswer();

            state._fsp--;

             after(grammarAccess.getOptionsAnswerRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOptionsAnswer"


    // $ANTLR start "ruleOptionsAnswer"
    // InternalXquiz.g:487:1: ruleOptionsAnswer : ( ( rule__OptionsAnswer__Alternatives ) ) ;
    public final void ruleOptionsAnswer() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:491:2: ( ( ( rule__OptionsAnswer__Alternatives ) ) )
            // InternalXquiz.g:492:2: ( ( rule__OptionsAnswer__Alternatives ) )
            {
            // InternalXquiz.g:492:2: ( ( rule__OptionsAnswer__Alternatives ) )
            // InternalXquiz.g:493:3: ( rule__OptionsAnswer__Alternatives )
            {
             before(grammarAccess.getOptionsAnswerAccess().getAlternatives()); 
            // InternalXquiz.g:494:3: ( rule__OptionsAnswer__Alternatives )
            // InternalXquiz.g:494:4: rule__OptionsAnswer__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__OptionsAnswer__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getOptionsAnswerAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOptionsAnswer"


    // $ANTLR start "entryRuleSingleOptionsAnswer"
    // InternalXquiz.g:503:1: entryRuleSingleOptionsAnswer : ruleSingleOptionsAnswer EOF ;
    public final void entryRuleSingleOptionsAnswer() throws RecognitionException {
        try {
            // InternalXquiz.g:504:1: ( ruleSingleOptionsAnswer EOF )
            // InternalXquiz.g:505:1: ruleSingleOptionsAnswer EOF
            {
             before(grammarAccess.getSingleOptionsAnswerRule()); 
            pushFollow(FOLLOW_1);
            ruleSingleOptionsAnswer();

            state._fsp--;

             after(grammarAccess.getSingleOptionsAnswerRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSingleOptionsAnswer"


    // $ANTLR start "ruleSingleOptionsAnswer"
    // InternalXquiz.g:512:1: ruleSingleOptionsAnswer : ( ( ( rule__SingleOptionsAnswer__OptionsAssignment ) ) ( ( rule__SingleOptionsAnswer__OptionsAssignment )* ) ) ;
    public final void ruleSingleOptionsAnswer() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:516:2: ( ( ( ( rule__SingleOptionsAnswer__OptionsAssignment ) ) ( ( rule__SingleOptionsAnswer__OptionsAssignment )* ) ) )
            // InternalXquiz.g:517:2: ( ( ( rule__SingleOptionsAnswer__OptionsAssignment ) ) ( ( rule__SingleOptionsAnswer__OptionsAssignment )* ) )
            {
            // InternalXquiz.g:517:2: ( ( ( rule__SingleOptionsAnswer__OptionsAssignment ) ) ( ( rule__SingleOptionsAnswer__OptionsAssignment )* ) )
            // InternalXquiz.g:518:3: ( ( rule__SingleOptionsAnswer__OptionsAssignment ) ) ( ( rule__SingleOptionsAnswer__OptionsAssignment )* )
            {
            // InternalXquiz.g:518:3: ( ( rule__SingleOptionsAnswer__OptionsAssignment ) )
            // InternalXquiz.g:519:4: ( rule__SingleOptionsAnswer__OptionsAssignment )
            {
             before(grammarAccess.getSingleOptionsAnswerAccess().getOptionsAssignment()); 
            // InternalXquiz.g:520:4: ( rule__SingleOptionsAnswer__OptionsAssignment )
            // InternalXquiz.g:520:5: rule__SingleOptionsAnswer__OptionsAssignment
            {
            pushFollow(FOLLOW_3);
            rule__SingleOptionsAnswer__OptionsAssignment();

            state._fsp--;


            }

             after(grammarAccess.getSingleOptionsAnswerAccess().getOptionsAssignment()); 

            }

            // InternalXquiz.g:523:3: ( ( rule__SingleOptionsAnswer__OptionsAssignment )* )
            // InternalXquiz.g:524:4: ( rule__SingleOptionsAnswer__OptionsAssignment )*
            {
             before(grammarAccess.getSingleOptionsAnswerAccess().getOptionsAssignment()); 
            // InternalXquiz.g:525:4: ( rule__SingleOptionsAnswer__OptionsAssignment )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==17) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalXquiz.g:525:5: rule__SingleOptionsAnswer__OptionsAssignment
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__SingleOptionsAnswer__OptionsAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getSingleOptionsAnswerAccess().getOptionsAssignment()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSingleOptionsAnswer"


    // $ANTLR start "entryRuleSingleListOption"
    // InternalXquiz.g:535:1: entryRuleSingleListOption : ruleSingleListOption EOF ;
    public final void entryRuleSingleListOption() throws RecognitionException {
        try {
            // InternalXquiz.g:536:1: ( ruleSingleListOption EOF )
            // InternalXquiz.g:537:1: ruleSingleListOption EOF
            {
             before(grammarAccess.getSingleListOptionRule()); 
            pushFollow(FOLLOW_1);
            ruleSingleListOption();

            state._fsp--;

             after(grammarAccess.getSingleListOptionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSingleListOption"


    // $ANTLR start "ruleSingleListOption"
    // InternalXquiz.g:544:1: ruleSingleListOption : ( ( rule__SingleListOption__Group__0 ) ) ;
    public final void ruleSingleListOption() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:548:2: ( ( ( rule__SingleListOption__Group__0 ) ) )
            // InternalXquiz.g:549:2: ( ( rule__SingleListOption__Group__0 ) )
            {
            // InternalXquiz.g:549:2: ( ( rule__SingleListOption__Group__0 ) )
            // InternalXquiz.g:550:3: ( rule__SingleListOption__Group__0 )
            {
             before(grammarAccess.getSingleListOptionAccess().getGroup()); 
            // InternalXquiz.g:551:3: ( rule__SingleListOption__Group__0 )
            // InternalXquiz.g:551:4: rule__SingleListOption__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__SingleListOption__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSingleListOptionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSingleListOption"


    // $ANTLR start "entryRuleManyOptionsAnswer"
    // InternalXquiz.g:560:1: entryRuleManyOptionsAnswer : ruleManyOptionsAnswer EOF ;
    public final void entryRuleManyOptionsAnswer() throws RecognitionException {
        try {
            // InternalXquiz.g:561:1: ( ruleManyOptionsAnswer EOF )
            // InternalXquiz.g:562:1: ruleManyOptionsAnswer EOF
            {
             before(grammarAccess.getManyOptionsAnswerRule()); 
            pushFollow(FOLLOW_1);
            ruleManyOptionsAnswer();

            state._fsp--;

             after(grammarAccess.getManyOptionsAnswerRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleManyOptionsAnswer"


    // $ANTLR start "ruleManyOptionsAnswer"
    // InternalXquiz.g:569:1: ruleManyOptionsAnswer : ( ( ( rule__ManyOptionsAnswer__OptionsAssignment ) ) ( ( rule__ManyOptionsAnswer__OptionsAssignment )* ) ) ;
    public final void ruleManyOptionsAnswer() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:573:2: ( ( ( ( rule__ManyOptionsAnswer__OptionsAssignment ) ) ( ( rule__ManyOptionsAnswer__OptionsAssignment )* ) ) )
            // InternalXquiz.g:574:2: ( ( ( rule__ManyOptionsAnswer__OptionsAssignment ) ) ( ( rule__ManyOptionsAnswer__OptionsAssignment )* ) )
            {
            // InternalXquiz.g:574:2: ( ( ( rule__ManyOptionsAnswer__OptionsAssignment ) ) ( ( rule__ManyOptionsAnswer__OptionsAssignment )* ) )
            // InternalXquiz.g:575:3: ( ( rule__ManyOptionsAnswer__OptionsAssignment ) ) ( ( rule__ManyOptionsAnswer__OptionsAssignment )* )
            {
            // InternalXquiz.g:575:3: ( ( rule__ManyOptionsAnswer__OptionsAssignment ) )
            // InternalXquiz.g:576:4: ( rule__ManyOptionsAnswer__OptionsAssignment )
            {
             before(grammarAccess.getManyOptionsAnswerAccess().getOptionsAssignment()); 
            // InternalXquiz.g:577:4: ( rule__ManyOptionsAnswer__OptionsAssignment )
            // InternalXquiz.g:577:5: rule__ManyOptionsAnswer__OptionsAssignment
            {
            pushFollow(FOLLOW_4);
            rule__ManyOptionsAnswer__OptionsAssignment();

            state._fsp--;


            }

             after(grammarAccess.getManyOptionsAnswerAccess().getOptionsAssignment()); 

            }

            // InternalXquiz.g:580:3: ( ( rule__ManyOptionsAnswer__OptionsAssignment )* )
            // InternalXquiz.g:581:4: ( rule__ManyOptionsAnswer__OptionsAssignment )*
            {
             before(grammarAccess.getManyOptionsAnswerAccess().getOptionsAssignment()); 
            // InternalXquiz.g:582:4: ( rule__ManyOptionsAnswer__OptionsAssignment )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==19) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalXquiz.g:582:5: rule__ManyOptionsAnswer__OptionsAssignment
            	    {
            	    pushFollow(FOLLOW_4);
            	    rule__ManyOptionsAnswer__OptionsAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

             after(grammarAccess.getManyOptionsAnswerAccess().getOptionsAssignment()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleManyOptionsAnswer"


    // $ANTLR start "entryRuleManyOption"
    // InternalXquiz.g:592:1: entryRuleManyOption : ruleManyOption EOF ;
    public final void entryRuleManyOption() throws RecognitionException {
        try {
            // InternalXquiz.g:593:1: ( ruleManyOption EOF )
            // InternalXquiz.g:594:1: ruleManyOption EOF
            {
             before(grammarAccess.getManyOptionRule()); 
            pushFollow(FOLLOW_1);
            ruleManyOption();

            state._fsp--;

             after(grammarAccess.getManyOptionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleManyOption"


    // $ANTLR start "ruleManyOption"
    // InternalXquiz.g:601:1: ruleManyOption : ( ( rule__ManyOption__Group__0 ) ) ;
    public final void ruleManyOption() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:605:2: ( ( ( rule__ManyOption__Group__0 ) ) )
            // InternalXquiz.g:606:2: ( ( rule__ManyOption__Group__0 ) )
            {
            // InternalXquiz.g:606:2: ( ( rule__ManyOption__Group__0 ) )
            // InternalXquiz.g:607:3: ( rule__ManyOption__Group__0 )
            {
             before(grammarAccess.getManyOptionAccess().getGroup()); 
            // InternalXquiz.g:608:3: ( rule__ManyOption__Group__0 )
            // InternalXquiz.g:608:4: rule__ManyOption__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ManyOption__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getManyOptionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleManyOption"


    // $ANTLR start "rule__AbstractQuizPart__Alternatives"
    // InternalXquiz.g:616:1: rule__AbstractQuizPart__Alternatives : ( ( ruleQuizPart ) | ( ruleQuizPartRef ) );
    public final void rule__AbstractQuizPart__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:620:1: ( ( ruleQuizPart ) | ( ruleQuizPartRef ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==14) ) {
                int LA3_1 = input.LA(2);

                if ( (LA3_1==15) ) {
                    alt3=2;
                }
                else if ( (LA3_1==RULE_ID) ) {
                    alt3=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 3, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalXquiz.g:621:2: ( ruleQuizPart )
                    {
                    // InternalXquiz.g:621:2: ( ruleQuizPart )
                    // InternalXquiz.g:622:3: ruleQuizPart
                    {
                     before(grammarAccess.getAbstractQuizPartAccess().getQuizPartParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleQuizPart();

                    state._fsp--;

                     after(grammarAccess.getAbstractQuizPartAccess().getQuizPartParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXquiz.g:627:2: ( ruleQuizPartRef )
                    {
                    // InternalXquiz.g:627:2: ( ruleQuizPartRef )
                    // InternalXquiz.g:628:3: ruleQuizPartRef
                    {
                     before(grammarAccess.getAbstractQuizPartAccess().getQuizPartRefParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleQuizPartRef();

                    state._fsp--;

                     after(grammarAccess.getAbstractQuizPartAccess().getQuizPartRefParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AbstractQuizPart__Alternatives"


    // $ANTLR start "rule__AbstractQA__Alternatives"
    // InternalXquiz.g:637:1: rule__AbstractQA__Alternatives : ( ( ruleQA ) | ( ruleQARef ) );
    public final void rule__AbstractQA__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:641:1: ( ( ruleQA ) | ( ruleQARef ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==RULE_ID||LA4_0==RULE_STRING) ) {
                alt4=1;
            }
            else if ( (LA4_0==15) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalXquiz.g:642:2: ( ruleQA )
                    {
                    // InternalXquiz.g:642:2: ( ruleQA )
                    // InternalXquiz.g:643:3: ruleQA
                    {
                     before(grammarAccess.getAbstractQAAccess().getQAParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleQA();

                    state._fsp--;

                     after(grammarAccess.getAbstractQAAccess().getQAParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXquiz.g:648:2: ( ruleQARef )
                    {
                    // InternalXquiz.g:648:2: ( ruleQARef )
                    // InternalXquiz.g:649:3: ruleQARef
                    {
                     before(grammarAccess.getAbstractQAAccess().getQARefParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleQARef();

                    state._fsp--;

                     after(grammarAccess.getAbstractQAAccess().getQARefParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AbstractQA__Alternatives"


    // $ANTLR start "rule__Answer__Alternatives"
    // InternalXquiz.g:658:1: rule__Answer__Alternatives : ( ( ruleOptionAnswer ) | ( ruleOptionsAnswer ) );
    public final void rule__Answer__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:662:1: ( ( ruleOptionAnswer ) | ( ruleOptionsAnswer ) )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( ((LA5_0>=RULE_INT && LA5_0<=RULE_STRING)||LA5_0==11||LA5_0==22) ) {
                alt5=1;
            }
            else if ( (LA5_0==17||LA5_0==19) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // InternalXquiz.g:663:2: ( ruleOptionAnswer )
                    {
                    // InternalXquiz.g:663:2: ( ruleOptionAnswer )
                    // InternalXquiz.g:664:3: ruleOptionAnswer
                    {
                     before(grammarAccess.getAnswerAccess().getOptionAnswerParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleOptionAnswer();

                    state._fsp--;

                     after(grammarAccess.getAnswerAccess().getOptionAnswerParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXquiz.g:669:2: ( ruleOptionsAnswer )
                    {
                    // InternalXquiz.g:669:2: ( ruleOptionsAnswer )
                    // InternalXquiz.g:670:3: ruleOptionsAnswer
                    {
                     before(grammarAccess.getAnswerAccess().getOptionsAnswerParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleOptionsAnswer();

                    state._fsp--;

                     after(grammarAccess.getAnswerAccess().getOptionsAnswerParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Answer__Alternatives"


    // $ANTLR start "rule__SimpleAnswer__Alternatives"
    // InternalXquiz.g:679:1: rule__SimpleAnswer__Alternatives : ( ( ruleStringAnswer ) | ( ruleNumberAnswer ) | ( ruleBooleanAnswer ) );
    public final void rule__SimpleAnswer__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:683:1: ( ( ruleStringAnswer ) | ( ruleNumberAnswer ) | ( ruleBooleanAnswer ) )
            int alt6=3;
            switch ( input.LA(1) ) {
            case RULE_STRING:
                {
                alt6=1;
                }
                break;
            case RULE_INT:
                {
                alt6=2;
                }
                break;
            case 11:
            case 22:
                {
                alt6=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // InternalXquiz.g:684:2: ( ruleStringAnswer )
                    {
                    // InternalXquiz.g:684:2: ( ruleStringAnswer )
                    // InternalXquiz.g:685:3: ruleStringAnswer
                    {
                     before(grammarAccess.getSimpleAnswerAccess().getStringAnswerParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleStringAnswer();

                    state._fsp--;

                     after(grammarAccess.getSimpleAnswerAccess().getStringAnswerParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXquiz.g:690:2: ( ruleNumberAnswer )
                    {
                    // InternalXquiz.g:690:2: ( ruleNumberAnswer )
                    // InternalXquiz.g:691:3: ruleNumberAnswer
                    {
                     before(grammarAccess.getSimpleAnswerAccess().getNumberAnswerParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleNumberAnswer();

                    state._fsp--;

                     after(grammarAccess.getSimpleAnswerAccess().getNumberAnswerParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalXquiz.g:696:2: ( ruleBooleanAnswer )
                    {
                    // InternalXquiz.g:696:2: ( ruleBooleanAnswer )
                    // InternalXquiz.g:697:3: ruleBooleanAnswer
                    {
                     before(grammarAccess.getSimpleAnswerAccess().getBooleanAnswerParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleBooleanAnswer();

                    state._fsp--;

                     after(grammarAccess.getSimpleAnswerAccess().getBooleanAnswerParserRuleCall_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SimpleAnswer__Alternatives"


    // $ANTLR start "rule__BooleanAnswer__Alternatives_1"
    // InternalXquiz.g:706:1: rule__BooleanAnswer__Alternatives_1 : ( ( ( rule__BooleanAnswer__ValueAssignment_1_0 ) ) | ( 'no' ) );
    public final void rule__BooleanAnswer__Alternatives_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:710:1: ( ( ( rule__BooleanAnswer__ValueAssignment_1_0 ) ) | ( 'no' ) )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==22) ) {
                alt7=1;
            }
            else if ( (LA7_0==11) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalXquiz.g:711:2: ( ( rule__BooleanAnswer__ValueAssignment_1_0 ) )
                    {
                    // InternalXquiz.g:711:2: ( ( rule__BooleanAnswer__ValueAssignment_1_0 ) )
                    // InternalXquiz.g:712:3: ( rule__BooleanAnswer__ValueAssignment_1_0 )
                    {
                     before(grammarAccess.getBooleanAnswerAccess().getValueAssignment_1_0()); 
                    // InternalXquiz.g:713:3: ( rule__BooleanAnswer__ValueAssignment_1_0 )
                    // InternalXquiz.g:713:4: rule__BooleanAnswer__ValueAssignment_1_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__BooleanAnswer__ValueAssignment_1_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getBooleanAnswerAccess().getValueAssignment_1_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXquiz.g:717:2: ( 'no' )
                    {
                    // InternalXquiz.g:717:2: ( 'no' )
                    // InternalXquiz.g:718:3: 'no'
                    {
                     before(grammarAccess.getBooleanAnswerAccess().getNoKeyword_1_1()); 
                    match(input,11,FOLLOW_2); 
                     after(grammarAccess.getBooleanAnswerAccess().getNoKeyword_1_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanAnswer__Alternatives_1"


    // $ANTLR start "rule__OptionsAnswer__Alternatives"
    // InternalXquiz.g:727:1: rule__OptionsAnswer__Alternatives : ( ( ruleSingleOptionsAnswer ) | ( ruleManyOptionsAnswer ) );
    public final void rule__OptionsAnswer__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:731:1: ( ( ruleSingleOptionsAnswer ) | ( ruleManyOptionsAnswer ) )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==17) ) {
                alt8=1;
            }
            else if ( (LA8_0==19) ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // InternalXquiz.g:732:2: ( ruleSingleOptionsAnswer )
                    {
                    // InternalXquiz.g:732:2: ( ruleSingleOptionsAnswer )
                    // InternalXquiz.g:733:3: ruleSingleOptionsAnswer
                    {
                     before(grammarAccess.getOptionsAnswerAccess().getSingleOptionsAnswerParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleSingleOptionsAnswer();

                    state._fsp--;

                     after(grammarAccess.getOptionsAnswerAccess().getSingleOptionsAnswerParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXquiz.g:738:2: ( ruleManyOptionsAnswer )
                    {
                    // InternalXquiz.g:738:2: ( ruleManyOptionsAnswer )
                    // InternalXquiz.g:739:3: ruleManyOptionsAnswer
                    {
                     before(grammarAccess.getOptionsAnswerAccess().getManyOptionsAnswerParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleManyOptionsAnswer();

                    state._fsp--;

                     after(grammarAccess.getOptionsAnswerAccess().getManyOptionsAnswerParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OptionsAnswer__Alternatives"


    // $ANTLR start "rule__Quiz__Group__0"
    // InternalXquiz.g:748:1: rule__Quiz__Group__0 : rule__Quiz__Group__0__Impl rule__Quiz__Group__1 ;
    public final void rule__Quiz__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:752:1: ( rule__Quiz__Group__0__Impl rule__Quiz__Group__1 )
            // InternalXquiz.g:753:2: rule__Quiz__Group__0__Impl rule__Quiz__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__Quiz__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Quiz__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Quiz__Group__0"


    // $ANTLR start "rule__Quiz__Group__0__Impl"
    // InternalXquiz.g:760:1: rule__Quiz__Group__0__Impl : ( 'quiz' ) ;
    public final void rule__Quiz__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:764:1: ( ( 'quiz' ) )
            // InternalXquiz.g:765:1: ( 'quiz' )
            {
            // InternalXquiz.g:765:1: ( 'quiz' )
            // InternalXquiz.g:766:2: 'quiz'
            {
             before(grammarAccess.getQuizAccess().getQuizKeyword_0()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getQuizAccess().getQuizKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Quiz__Group__0__Impl"


    // $ANTLR start "rule__Quiz__Group__1"
    // InternalXquiz.g:775:1: rule__Quiz__Group__1 : rule__Quiz__Group__1__Impl rule__Quiz__Group__2 ;
    public final void rule__Quiz__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:779:1: ( rule__Quiz__Group__1__Impl rule__Quiz__Group__2 )
            // InternalXquiz.g:780:2: rule__Quiz__Group__1__Impl rule__Quiz__Group__2
            {
            pushFollow(FOLLOW_6);
            rule__Quiz__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Quiz__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Quiz__Group__1"


    // $ANTLR start "rule__Quiz__Group__1__Impl"
    // InternalXquiz.g:787:1: rule__Quiz__Group__1__Impl : ( ( rule__Quiz__NameAssignment_1 ) ) ;
    public final void rule__Quiz__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:791:1: ( ( ( rule__Quiz__NameAssignment_1 ) ) )
            // InternalXquiz.g:792:1: ( ( rule__Quiz__NameAssignment_1 ) )
            {
            // InternalXquiz.g:792:1: ( ( rule__Quiz__NameAssignment_1 ) )
            // InternalXquiz.g:793:2: ( rule__Quiz__NameAssignment_1 )
            {
             before(grammarAccess.getQuizAccess().getNameAssignment_1()); 
            // InternalXquiz.g:794:2: ( rule__Quiz__NameAssignment_1 )
            // InternalXquiz.g:794:3: rule__Quiz__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Quiz__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getQuizAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Quiz__Group__1__Impl"


    // $ANTLR start "rule__Quiz__Group__2"
    // InternalXquiz.g:802:1: rule__Quiz__Group__2 : rule__Quiz__Group__2__Impl rule__Quiz__Group__3 ;
    public final void rule__Quiz__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:806:1: ( rule__Quiz__Group__2__Impl rule__Quiz__Group__3 )
            // InternalXquiz.g:807:2: rule__Quiz__Group__2__Impl rule__Quiz__Group__3
            {
            pushFollow(FOLLOW_6);
            rule__Quiz__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Quiz__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Quiz__Group__2"


    // $ANTLR start "rule__Quiz__Group__2__Impl"
    // InternalXquiz.g:814:1: rule__Quiz__Group__2__Impl : ( ( rule__Quiz__TitleAssignment_2 )? ) ;
    public final void rule__Quiz__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:818:1: ( ( ( rule__Quiz__TitleAssignment_2 )? ) )
            // InternalXquiz.g:819:1: ( ( rule__Quiz__TitleAssignment_2 )? )
            {
            // InternalXquiz.g:819:1: ( ( rule__Quiz__TitleAssignment_2 )? )
            // InternalXquiz.g:820:2: ( rule__Quiz__TitleAssignment_2 )?
            {
             before(grammarAccess.getQuizAccess().getTitleAssignment_2()); 
            // InternalXquiz.g:821:2: ( rule__Quiz__TitleAssignment_2 )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==RULE_STRING) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalXquiz.g:821:3: rule__Quiz__TitleAssignment_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__Quiz__TitleAssignment_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getQuizAccess().getTitleAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Quiz__Group__2__Impl"


    // $ANTLR start "rule__Quiz__Group__3"
    // InternalXquiz.g:829:1: rule__Quiz__Group__3 : rule__Quiz__Group__3__Impl ;
    public final void rule__Quiz__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:833:1: ( rule__Quiz__Group__3__Impl )
            // InternalXquiz.g:834:2: rule__Quiz__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Quiz__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Quiz__Group__3"


    // $ANTLR start "rule__Quiz__Group__3__Impl"
    // InternalXquiz.g:840:1: rule__Quiz__Group__3__Impl : ( ( rule__Quiz__PartsAssignment_3 )* ) ;
    public final void rule__Quiz__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:844:1: ( ( ( rule__Quiz__PartsAssignment_3 )* ) )
            // InternalXquiz.g:845:1: ( ( rule__Quiz__PartsAssignment_3 )* )
            {
            // InternalXquiz.g:845:1: ( ( rule__Quiz__PartsAssignment_3 )* )
            // InternalXquiz.g:846:2: ( rule__Quiz__PartsAssignment_3 )*
            {
             before(grammarAccess.getQuizAccess().getPartsAssignment_3()); 
            // InternalXquiz.g:847:2: ( rule__Quiz__PartsAssignment_3 )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==14) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalXquiz.g:847:3: rule__Quiz__PartsAssignment_3
            	    {
            	    pushFollow(FOLLOW_7);
            	    rule__Quiz__PartsAssignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

             after(grammarAccess.getQuizAccess().getPartsAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Quiz__Group__3__Impl"


    // $ANTLR start "rule__QName__Group__0"
    // InternalXquiz.g:856:1: rule__QName__Group__0 : rule__QName__Group__0__Impl rule__QName__Group__1 ;
    public final void rule__QName__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:860:1: ( rule__QName__Group__0__Impl rule__QName__Group__1 )
            // InternalXquiz.g:861:2: rule__QName__Group__0__Impl rule__QName__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__QName__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QName__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QName__Group__0"


    // $ANTLR start "rule__QName__Group__0__Impl"
    // InternalXquiz.g:868:1: rule__QName__Group__0__Impl : ( RULE_ID ) ;
    public final void rule__QName__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:872:1: ( ( RULE_ID ) )
            // InternalXquiz.g:873:1: ( RULE_ID )
            {
            // InternalXquiz.g:873:1: ( RULE_ID )
            // InternalXquiz.g:874:2: RULE_ID
            {
             before(grammarAccess.getQNameAccess().getIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getQNameAccess().getIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QName__Group__0__Impl"


    // $ANTLR start "rule__QName__Group__1"
    // InternalXquiz.g:883:1: rule__QName__Group__1 : rule__QName__Group__1__Impl ;
    public final void rule__QName__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:887:1: ( rule__QName__Group__1__Impl )
            // InternalXquiz.g:888:2: rule__QName__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QName__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QName__Group__1"


    // $ANTLR start "rule__QName__Group__1__Impl"
    // InternalXquiz.g:894:1: rule__QName__Group__1__Impl : ( ( rule__QName__Group_1__0 )* ) ;
    public final void rule__QName__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:898:1: ( ( ( rule__QName__Group_1__0 )* ) )
            // InternalXquiz.g:899:1: ( ( rule__QName__Group_1__0 )* )
            {
            // InternalXquiz.g:899:1: ( ( rule__QName__Group_1__0 )* )
            // InternalXquiz.g:900:2: ( rule__QName__Group_1__0 )*
            {
             before(grammarAccess.getQNameAccess().getGroup_1()); 
            // InternalXquiz.g:901:2: ( rule__QName__Group_1__0 )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==13) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalXquiz.g:901:3: rule__QName__Group_1__0
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__QName__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

             after(grammarAccess.getQNameAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QName__Group__1__Impl"


    // $ANTLR start "rule__QName__Group_1__0"
    // InternalXquiz.g:910:1: rule__QName__Group_1__0 : rule__QName__Group_1__0__Impl rule__QName__Group_1__1 ;
    public final void rule__QName__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:914:1: ( rule__QName__Group_1__0__Impl rule__QName__Group_1__1 )
            // InternalXquiz.g:915:2: rule__QName__Group_1__0__Impl rule__QName__Group_1__1
            {
            pushFollow(FOLLOW_5);
            rule__QName__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QName__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QName__Group_1__0"


    // $ANTLR start "rule__QName__Group_1__0__Impl"
    // InternalXquiz.g:922:1: rule__QName__Group_1__0__Impl : ( '.' ) ;
    public final void rule__QName__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:926:1: ( ( '.' ) )
            // InternalXquiz.g:927:1: ( '.' )
            {
            // InternalXquiz.g:927:1: ( '.' )
            // InternalXquiz.g:928:2: '.'
            {
             before(grammarAccess.getQNameAccess().getFullStopKeyword_1_0()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getQNameAccess().getFullStopKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QName__Group_1__0__Impl"


    // $ANTLR start "rule__QName__Group_1__1"
    // InternalXquiz.g:937:1: rule__QName__Group_1__1 : rule__QName__Group_1__1__Impl ;
    public final void rule__QName__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:941:1: ( rule__QName__Group_1__1__Impl )
            // InternalXquiz.g:942:2: rule__QName__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QName__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QName__Group_1__1"


    // $ANTLR start "rule__QName__Group_1__1__Impl"
    // InternalXquiz.g:948:1: rule__QName__Group_1__1__Impl : ( RULE_ID ) ;
    public final void rule__QName__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:952:1: ( ( RULE_ID ) )
            // InternalXquiz.g:953:1: ( RULE_ID )
            {
            // InternalXquiz.g:953:1: ( RULE_ID )
            // InternalXquiz.g:954:2: RULE_ID
            {
             before(grammarAccess.getQNameAccess().getIDTerminalRuleCall_1_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getQNameAccess().getIDTerminalRuleCall_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QName__Group_1__1__Impl"


    // $ANTLR start "rule__QuizPartRef__Group__0"
    // InternalXquiz.g:964:1: rule__QuizPartRef__Group__0 : rule__QuizPartRef__Group__0__Impl rule__QuizPartRef__Group__1 ;
    public final void rule__QuizPartRef__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:968:1: ( rule__QuizPartRef__Group__0__Impl rule__QuizPartRef__Group__1 )
            // InternalXquiz.g:969:2: rule__QuizPartRef__Group__0__Impl rule__QuizPartRef__Group__1
            {
            pushFollow(FOLLOW_10);
            rule__QuizPartRef__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QuizPartRef__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuizPartRef__Group__0"


    // $ANTLR start "rule__QuizPartRef__Group__0__Impl"
    // InternalXquiz.g:976:1: rule__QuizPartRef__Group__0__Impl : ( 'part' ) ;
    public final void rule__QuizPartRef__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:980:1: ( ( 'part' ) )
            // InternalXquiz.g:981:1: ( 'part' )
            {
            // InternalXquiz.g:981:1: ( 'part' )
            // InternalXquiz.g:982:2: 'part'
            {
             before(grammarAccess.getQuizPartRefAccess().getPartKeyword_0()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getQuizPartRefAccess().getPartKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuizPartRef__Group__0__Impl"


    // $ANTLR start "rule__QuizPartRef__Group__1"
    // InternalXquiz.g:991:1: rule__QuizPartRef__Group__1 : rule__QuizPartRef__Group__1__Impl rule__QuizPartRef__Group__2 ;
    public final void rule__QuizPartRef__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:995:1: ( rule__QuizPartRef__Group__1__Impl rule__QuizPartRef__Group__2 )
            // InternalXquiz.g:996:2: rule__QuizPartRef__Group__1__Impl rule__QuizPartRef__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__QuizPartRef__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QuizPartRef__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuizPartRef__Group__1"


    // $ANTLR start "rule__QuizPartRef__Group__1__Impl"
    // InternalXquiz.g:1003:1: rule__QuizPartRef__Group__1__Impl : ( '@' ) ;
    public final void rule__QuizPartRef__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1007:1: ( ( '@' ) )
            // InternalXquiz.g:1008:1: ( '@' )
            {
            // InternalXquiz.g:1008:1: ( '@' )
            // InternalXquiz.g:1009:2: '@'
            {
             before(grammarAccess.getQuizPartRefAccess().getCommercialAtKeyword_1()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getQuizPartRefAccess().getCommercialAtKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuizPartRef__Group__1__Impl"


    // $ANTLR start "rule__QuizPartRef__Group__2"
    // InternalXquiz.g:1018:1: rule__QuizPartRef__Group__2 : rule__QuizPartRef__Group__2__Impl ;
    public final void rule__QuizPartRef__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1022:1: ( rule__QuizPartRef__Group__2__Impl )
            // InternalXquiz.g:1023:2: rule__QuizPartRef__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QuizPartRef__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuizPartRef__Group__2"


    // $ANTLR start "rule__QuizPartRef__Group__2__Impl"
    // InternalXquiz.g:1029:1: rule__QuizPartRef__Group__2__Impl : ( ( rule__QuizPartRef__PartRefAssignment_2 ) ) ;
    public final void rule__QuizPartRef__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1033:1: ( ( ( rule__QuizPartRef__PartRefAssignment_2 ) ) )
            // InternalXquiz.g:1034:1: ( ( rule__QuizPartRef__PartRefAssignment_2 ) )
            {
            // InternalXquiz.g:1034:1: ( ( rule__QuizPartRef__PartRefAssignment_2 ) )
            // InternalXquiz.g:1035:2: ( rule__QuizPartRef__PartRefAssignment_2 )
            {
             before(grammarAccess.getQuizPartRefAccess().getPartRefAssignment_2()); 
            // InternalXquiz.g:1036:2: ( rule__QuizPartRef__PartRefAssignment_2 )
            // InternalXquiz.g:1036:3: rule__QuizPartRef__PartRefAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__QuizPartRef__PartRefAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getQuizPartRefAccess().getPartRefAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuizPartRef__Group__2__Impl"


    // $ANTLR start "rule__QuizPart__Group__0"
    // InternalXquiz.g:1045:1: rule__QuizPart__Group__0 : rule__QuizPart__Group__0__Impl rule__QuizPart__Group__1 ;
    public final void rule__QuizPart__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1049:1: ( rule__QuizPart__Group__0__Impl rule__QuizPart__Group__1 )
            // InternalXquiz.g:1050:2: rule__QuizPart__Group__0__Impl rule__QuizPart__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__QuizPart__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QuizPart__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuizPart__Group__0"


    // $ANTLR start "rule__QuizPart__Group__0__Impl"
    // InternalXquiz.g:1057:1: rule__QuizPart__Group__0__Impl : ( 'part' ) ;
    public final void rule__QuizPart__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1061:1: ( ( 'part' ) )
            // InternalXquiz.g:1062:1: ( 'part' )
            {
            // InternalXquiz.g:1062:1: ( 'part' )
            // InternalXquiz.g:1063:2: 'part'
            {
             before(grammarAccess.getQuizPartAccess().getPartKeyword_0()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getQuizPartAccess().getPartKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuizPart__Group__0__Impl"


    // $ANTLR start "rule__QuizPart__Group__1"
    // InternalXquiz.g:1072:1: rule__QuizPart__Group__1 : rule__QuizPart__Group__1__Impl rule__QuizPart__Group__2 ;
    public final void rule__QuizPart__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1076:1: ( rule__QuizPart__Group__1__Impl rule__QuizPart__Group__2 )
            // InternalXquiz.g:1077:2: rule__QuizPart__Group__1__Impl rule__QuizPart__Group__2
            {
            pushFollow(FOLLOW_11);
            rule__QuizPart__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QuizPart__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuizPart__Group__1"


    // $ANTLR start "rule__QuizPart__Group__1__Impl"
    // InternalXquiz.g:1084:1: rule__QuizPart__Group__1__Impl : ( ( rule__QuizPart__NameAssignment_1 ) ) ;
    public final void rule__QuizPart__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1088:1: ( ( ( rule__QuizPart__NameAssignment_1 ) ) )
            // InternalXquiz.g:1089:1: ( ( rule__QuizPart__NameAssignment_1 ) )
            {
            // InternalXquiz.g:1089:1: ( ( rule__QuizPart__NameAssignment_1 ) )
            // InternalXquiz.g:1090:2: ( rule__QuizPart__NameAssignment_1 )
            {
             before(grammarAccess.getQuizPartAccess().getNameAssignment_1()); 
            // InternalXquiz.g:1091:2: ( rule__QuizPart__NameAssignment_1 )
            // InternalXquiz.g:1091:3: rule__QuizPart__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__QuizPart__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getQuizPartAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuizPart__Group__1__Impl"


    // $ANTLR start "rule__QuizPart__Group__2"
    // InternalXquiz.g:1099:1: rule__QuizPart__Group__2 : rule__QuizPart__Group__2__Impl rule__QuizPart__Group__3 ;
    public final void rule__QuizPart__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1103:1: ( rule__QuizPart__Group__2__Impl rule__QuizPart__Group__3 )
            // InternalXquiz.g:1104:2: rule__QuizPart__Group__2__Impl rule__QuizPart__Group__3
            {
            pushFollow(FOLLOW_12);
            rule__QuizPart__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QuizPart__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuizPart__Group__2"


    // $ANTLR start "rule__QuizPart__Group__2__Impl"
    // InternalXquiz.g:1111:1: rule__QuizPart__Group__2__Impl : ( ( rule__QuizPart__TitleAssignment_2 ) ) ;
    public final void rule__QuizPart__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1115:1: ( ( ( rule__QuizPart__TitleAssignment_2 ) ) )
            // InternalXquiz.g:1116:1: ( ( rule__QuizPart__TitleAssignment_2 ) )
            {
            // InternalXquiz.g:1116:1: ( ( rule__QuizPart__TitleAssignment_2 ) )
            // InternalXquiz.g:1117:2: ( rule__QuizPart__TitleAssignment_2 )
            {
             before(grammarAccess.getQuizPartAccess().getTitleAssignment_2()); 
            // InternalXquiz.g:1118:2: ( rule__QuizPart__TitleAssignment_2 )
            // InternalXquiz.g:1118:3: rule__QuizPart__TitleAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__QuizPart__TitleAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getQuizPartAccess().getTitleAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuizPart__Group__2__Impl"


    // $ANTLR start "rule__QuizPart__Group__3"
    // InternalXquiz.g:1126:1: rule__QuizPart__Group__3 : rule__QuizPart__Group__3__Impl ;
    public final void rule__QuizPart__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1130:1: ( rule__QuizPart__Group__3__Impl )
            // InternalXquiz.g:1131:2: rule__QuizPart__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QuizPart__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuizPart__Group__3"


    // $ANTLR start "rule__QuizPart__Group__3__Impl"
    // InternalXquiz.g:1137:1: rule__QuizPart__Group__3__Impl : ( ( rule__QuizPart__QuestionsAssignment_3 )* ) ;
    public final void rule__QuizPart__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1141:1: ( ( ( rule__QuizPart__QuestionsAssignment_3 )* ) )
            // InternalXquiz.g:1142:1: ( ( rule__QuizPart__QuestionsAssignment_3 )* )
            {
            // InternalXquiz.g:1142:1: ( ( rule__QuizPart__QuestionsAssignment_3 )* )
            // InternalXquiz.g:1143:2: ( rule__QuizPart__QuestionsAssignment_3 )*
            {
             before(grammarAccess.getQuizPartAccess().getQuestionsAssignment_3()); 
            // InternalXquiz.g:1144:2: ( rule__QuizPart__QuestionsAssignment_3 )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==RULE_ID||LA12_0==RULE_STRING||LA12_0==15) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalXquiz.g:1144:3: rule__QuizPart__QuestionsAssignment_3
            	    {
            	    pushFollow(FOLLOW_13);
            	    rule__QuizPart__QuestionsAssignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

             after(grammarAccess.getQuizPartAccess().getQuestionsAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuizPart__Group__3__Impl"


    // $ANTLR start "rule__QARef__Group__0"
    // InternalXquiz.g:1153:1: rule__QARef__Group__0 : rule__QARef__Group__0__Impl rule__QARef__Group__1 ;
    public final void rule__QARef__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1157:1: ( rule__QARef__Group__0__Impl rule__QARef__Group__1 )
            // InternalXquiz.g:1158:2: rule__QARef__Group__0__Impl rule__QARef__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__QARef__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QARef__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QARef__Group__0"


    // $ANTLR start "rule__QARef__Group__0__Impl"
    // InternalXquiz.g:1165:1: rule__QARef__Group__0__Impl : ( '@' ) ;
    public final void rule__QARef__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1169:1: ( ( '@' ) )
            // InternalXquiz.g:1170:1: ( '@' )
            {
            // InternalXquiz.g:1170:1: ( '@' )
            // InternalXquiz.g:1171:2: '@'
            {
             before(grammarAccess.getQARefAccess().getCommercialAtKeyword_0()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getQARefAccess().getCommercialAtKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QARef__Group__0__Impl"


    // $ANTLR start "rule__QARef__Group__1"
    // InternalXquiz.g:1180:1: rule__QARef__Group__1 : rule__QARef__Group__1__Impl ;
    public final void rule__QARef__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1184:1: ( rule__QARef__Group__1__Impl )
            // InternalXquiz.g:1185:2: rule__QARef__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QARef__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QARef__Group__1"


    // $ANTLR start "rule__QARef__Group__1__Impl"
    // InternalXquiz.g:1191:1: rule__QARef__Group__1__Impl : ( ( rule__QARef__QaRefAssignment_1 ) ) ;
    public final void rule__QARef__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1195:1: ( ( ( rule__QARef__QaRefAssignment_1 ) ) )
            // InternalXquiz.g:1196:1: ( ( rule__QARef__QaRefAssignment_1 ) )
            {
            // InternalXquiz.g:1196:1: ( ( rule__QARef__QaRefAssignment_1 ) )
            // InternalXquiz.g:1197:2: ( rule__QARef__QaRefAssignment_1 )
            {
             before(grammarAccess.getQARefAccess().getQaRefAssignment_1()); 
            // InternalXquiz.g:1198:2: ( rule__QARef__QaRefAssignment_1 )
            // InternalXquiz.g:1198:3: rule__QARef__QaRefAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__QARef__QaRefAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getQARefAccess().getQaRefAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QARef__Group__1__Impl"


    // $ANTLR start "rule__QA__Group__0"
    // InternalXquiz.g:1207:1: rule__QA__Group__0 : rule__QA__Group__0__Impl rule__QA__Group__1 ;
    public final void rule__QA__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1211:1: ( rule__QA__Group__0__Impl rule__QA__Group__1 )
            // InternalXquiz.g:1212:2: rule__QA__Group__0__Impl rule__QA__Group__1
            {
            pushFollow(FOLLOW_14);
            rule__QA__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QA__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QA__Group__0"


    // $ANTLR start "rule__QA__Group__0__Impl"
    // InternalXquiz.g:1219:1: rule__QA__Group__0__Impl : ( ( rule__QA__NameAssignment_0 )? ) ;
    public final void rule__QA__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1223:1: ( ( ( rule__QA__NameAssignment_0 )? ) )
            // InternalXquiz.g:1224:1: ( ( rule__QA__NameAssignment_0 )? )
            {
            // InternalXquiz.g:1224:1: ( ( rule__QA__NameAssignment_0 )? )
            // InternalXquiz.g:1225:2: ( rule__QA__NameAssignment_0 )?
            {
             before(grammarAccess.getQAAccess().getNameAssignment_0()); 
            // InternalXquiz.g:1226:2: ( rule__QA__NameAssignment_0 )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==RULE_ID) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalXquiz.g:1226:3: rule__QA__NameAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__QA__NameAssignment_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getQAAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QA__Group__0__Impl"


    // $ANTLR start "rule__QA__Group__1"
    // InternalXquiz.g:1234:1: rule__QA__Group__1 : rule__QA__Group__1__Impl rule__QA__Group__2 ;
    public final void rule__QA__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1238:1: ( rule__QA__Group__1__Impl rule__QA__Group__2 )
            // InternalXquiz.g:1239:2: rule__QA__Group__1__Impl rule__QA__Group__2
            {
            pushFollow(FOLLOW_15);
            rule__QA__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QA__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QA__Group__1"


    // $ANTLR start "rule__QA__Group__1__Impl"
    // InternalXquiz.g:1246:1: rule__QA__Group__1__Impl : ( ( rule__QA__QAssignment_1 ) ) ;
    public final void rule__QA__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1250:1: ( ( ( rule__QA__QAssignment_1 ) ) )
            // InternalXquiz.g:1251:1: ( ( rule__QA__QAssignment_1 ) )
            {
            // InternalXquiz.g:1251:1: ( ( rule__QA__QAssignment_1 ) )
            // InternalXquiz.g:1252:2: ( rule__QA__QAssignment_1 )
            {
             before(grammarAccess.getQAAccess().getQAssignment_1()); 
            // InternalXquiz.g:1253:2: ( rule__QA__QAssignment_1 )
            // InternalXquiz.g:1253:3: rule__QA__QAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__QA__QAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getQAAccess().getQAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QA__Group__1__Impl"


    // $ANTLR start "rule__QA__Group__2"
    // InternalXquiz.g:1261:1: rule__QA__Group__2 : rule__QA__Group__2__Impl rule__QA__Group__3 ;
    public final void rule__QA__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1265:1: ( rule__QA__Group__2__Impl rule__QA__Group__3 )
            // InternalXquiz.g:1266:2: rule__QA__Group__2__Impl rule__QA__Group__3
            {
            pushFollow(FOLLOW_15);
            rule__QA__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QA__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QA__Group__2"


    // $ANTLR start "rule__QA__Group__2__Impl"
    // InternalXquiz.g:1273:1: rule__QA__Group__2__Impl : ( ( '?' )? ) ;
    public final void rule__QA__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1277:1: ( ( ( '?' )? ) )
            // InternalXquiz.g:1278:1: ( ( '?' )? )
            {
            // InternalXquiz.g:1278:1: ( ( '?' )? )
            // InternalXquiz.g:1279:2: ( '?' )?
            {
             before(grammarAccess.getQAAccess().getQuestionMarkKeyword_2()); 
            // InternalXquiz.g:1280:2: ( '?' )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==16) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalXquiz.g:1280:3: '?'
                    {
                    match(input,16,FOLLOW_2); 

                    }
                    break;

            }

             after(grammarAccess.getQAAccess().getQuestionMarkKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QA__Group__2__Impl"


    // $ANTLR start "rule__QA__Group__3"
    // InternalXquiz.g:1288:1: rule__QA__Group__3 : rule__QA__Group__3__Impl ;
    public final void rule__QA__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1292:1: ( rule__QA__Group__3__Impl )
            // InternalXquiz.g:1293:2: rule__QA__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QA__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QA__Group__3"


    // $ANTLR start "rule__QA__Group__3__Impl"
    // InternalXquiz.g:1299:1: rule__QA__Group__3__Impl : ( ( rule__QA__AAssignment_3 ) ) ;
    public final void rule__QA__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1303:1: ( ( ( rule__QA__AAssignment_3 ) ) )
            // InternalXquiz.g:1304:1: ( ( rule__QA__AAssignment_3 ) )
            {
            // InternalXquiz.g:1304:1: ( ( rule__QA__AAssignment_3 ) )
            // InternalXquiz.g:1305:2: ( rule__QA__AAssignment_3 )
            {
             before(grammarAccess.getQAAccess().getAAssignment_3()); 
            // InternalXquiz.g:1306:2: ( rule__QA__AAssignment_3 )
            // InternalXquiz.g:1306:3: rule__QA__AAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__QA__AAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getQAAccess().getAAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QA__Group__3__Impl"


    // $ANTLR start "rule__EDouble__Group__0"
    // InternalXquiz.g:1315:1: rule__EDouble__Group__0 : rule__EDouble__Group__0__Impl rule__EDouble__Group__1 ;
    public final void rule__EDouble__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1319:1: ( rule__EDouble__Group__0__Impl rule__EDouble__Group__1 )
            // InternalXquiz.g:1320:2: rule__EDouble__Group__0__Impl rule__EDouble__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__EDouble__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__EDouble__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EDouble__Group__0"


    // $ANTLR start "rule__EDouble__Group__0__Impl"
    // InternalXquiz.g:1327:1: rule__EDouble__Group__0__Impl : ( RULE_INT ) ;
    public final void rule__EDouble__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1331:1: ( ( RULE_INT ) )
            // InternalXquiz.g:1332:1: ( RULE_INT )
            {
            // InternalXquiz.g:1332:1: ( RULE_INT )
            // InternalXquiz.g:1333:2: RULE_INT
            {
             before(grammarAccess.getEDoubleAccess().getINTTerminalRuleCall_0()); 
            match(input,RULE_INT,FOLLOW_2); 
             after(grammarAccess.getEDoubleAccess().getINTTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EDouble__Group__0__Impl"


    // $ANTLR start "rule__EDouble__Group__1"
    // InternalXquiz.g:1342:1: rule__EDouble__Group__1 : rule__EDouble__Group__1__Impl ;
    public final void rule__EDouble__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1346:1: ( rule__EDouble__Group__1__Impl )
            // InternalXquiz.g:1347:2: rule__EDouble__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__EDouble__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EDouble__Group__1"


    // $ANTLR start "rule__EDouble__Group__1__Impl"
    // InternalXquiz.g:1353:1: rule__EDouble__Group__1__Impl : ( ( rule__EDouble__Group_1__0 )? ) ;
    public final void rule__EDouble__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1357:1: ( ( ( rule__EDouble__Group_1__0 )? ) )
            // InternalXquiz.g:1358:1: ( ( rule__EDouble__Group_1__0 )? )
            {
            // InternalXquiz.g:1358:1: ( ( rule__EDouble__Group_1__0 )? )
            // InternalXquiz.g:1359:2: ( rule__EDouble__Group_1__0 )?
            {
             before(grammarAccess.getEDoubleAccess().getGroup_1()); 
            // InternalXquiz.g:1360:2: ( rule__EDouble__Group_1__0 )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==13) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalXquiz.g:1360:3: rule__EDouble__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__EDouble__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getEDoubleAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EDouble__Group__1__Impl"


    // $ANTLR start "rule__EDouble__Group_1__0"
    // InternalXquiz.g:1369:1: rule__EDouble__Group_1__0 : rule__EDouble__Group_1__0__Impl rule__EDouble__Group_1__1 ;
    public final void rule__EDouble__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1373:1: ( rule__EDouble__Group_1__0__Impl rule__EDouble__Group_1__1 )
            // InternalXquiz.g:1374:2: rule__EDouble__Group_1__0__Impl rule__EDouble__Group_1__1
            {
            pushFollow(FOLLOW_16);
            rule__EDouble__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__EDouble__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EDouble__Group_1__0"


    // $ANTLR start "rule__EDouble__Group_1__0__Impl"
    // InternalXquiz.g:1381:1: rule__EDouble__Group_1__0__Impl : ( '.' ) ;
    public final void rule__EDouble__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1385:1: ( ( '.' ) )
            // InternalXquiz.g:1386:1: ( '.' )
            {
            // InternalXquiz.g:1386:1: ( '.' )
            // InternalXquiz.g:1387:2: '.'
            {
             before(grammarAccess.getEDoubleAccess().getFullStopKeyword_1_0()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getEDoubleAccess().getFullStopKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EDouble__Group_1__0__Impl"


    // $ANTLR start "rule__EDouble__Group_1__1"
    // InternalXquiz.g:1396:1: rule__EDouble__Group_1__1 : rule__EDouble__Group_1__1__Impl ;
    public final void rule__EDouble__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1400:1: ( rule__EDouble__Group_1__1__Impl )
            // InternalXquiz.g:1401:2: rule__EDouble__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__EDouble__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EDouble__Group_1__1"


    // $ANTLR start "rule__EDouble__Group_1__1__Impl"
    // InternalXquiz.g:1407:1: rule__EDouble__Group_1__1__Impl : ( RULE_INT ) ;
    public final void rule__EDouble__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1411:1: ( ( RULE_INT ) )
            // InternalXquiz.g:1412:1: ( RULE_INT )
            {
            // InternalXquiz.g:1412:1: ( RULE_INT )
            // InternalXquiz.g:1413:2: RULE_INT
            {
             before(grammarAccess.getEDoubleAccess().getINTTerminalRuleCall_1_1()); 
            match(input,RULE_INT,FOLLOW_2); 
             after(grammarAccess.getEDoubleAccess().getINTTerminalRuleCall_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EDouble__Group_1__1__Impl"


    // $ANTLR start "rule__StringAnswer__Group__0"
    // InternalXquiz.g:1423:1: rule__StringAnswer__Group__0 : rule__StringAnswer__Group__0__Impl rule__StringAnswer__Group__1 ;
    public final void rule__StringAnswer__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1427:1: ( rule__StringAnswer__Group__0__Impl rule__StringAnswer__Group__1 )
            // InternalXquiz.g:1428:2: rule__StringAnswer__Group__0__Impl rule__StringAnswer__Group__1
            {
            pushFollow(FOLLOW_17);
            rule__StringAnswer__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StringAnswer__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringAnswer__Group__0"


    // $ANTLR start "rule__StringAnswer__Group__0__Impl"
    // InternalXquiz.g:1435:1: rule__StringAnswer__Group__0__Impl : ( ( rule__StringAnswer__ValueAssignment_0 ) ) ;
    public final void rule__StringAnswer__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1439:1: ( ( ( rule__StringAnswer__ValueAssignment_0 ) ) )
            // InternalXquiz.g:1440:1: ( ( rule__StringAnswer__ValueAssignment_0 ) )
            {
            // InternalXquiz.g:1440:1: ( ( rule__StringAnswer__ValueAssignment_0 ) )
            // InternalXquiz.g:1441:2: ( rule__StringAnswer__ValueAssignment_0 )
            {
             before(grammarAccess.getStringAnswerAccess().getValueAssignment_0()); 
            // InternalXquiz.g:1442:2: ( rule__StringAnswer__ValueAssignment_0 )
            // InternalXquiz.g:1442:3: rule__StringAnswer__ValueAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__StringAnswer__ValueAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getStringAnswerAccess().getValueAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringAnswer__Group__0__Impl"


    // $ANTLR start "rule__StringAnswer__Group__1"
    // InternalXquiz.g:1450:1: rule__StringAnswer__Group__1 : rule__StringAnswer__Group__1__Impl ;
    public final void rule__StringAnswer__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1454:1: ( rule__StringAnswer__Group__1__Impl )
            // InternalXquiz.g:1455:2: rule__StringAnswer__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__StringAnswer__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringAnswer__Group__1"


    // $ANTLR start "rule__StringAnswer__Group__1__Impl"
    // InternalXquiz.g:1461:1: rule__StringAnswer__Group__1__Impl : ( ( rule__StringAnswer__RegexpAssignment_1 )? ) ;
    public final void rule__StringAnswer__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1465:1: ( ( ( rule__StringAnswer__RegexpAssignment_1 )? ) )
            // InternalXquiz.g:1466:1: ( ( rule__StringAnswer__RegexpAssignment_1 )? )
            {
            // InternalXquiz.g:1466:1: ( ( rule__StringAnswer__RegexpAssignment_1 )? )
            // InternalXquiz.g:1467:2: ( rule__StringAnswer__RegexpAssignment_1 )?
            {
             before(grammarAccess.getStringAnswerAccess().getRegexpAssignment_1()); 
            // InternalXquiz.g:1468:2: ( rule__StringAnswer__RegexpAssignment_1 )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==21) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalXquiz.g:1468:3: rule__StringAnswer__RegexpAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__StringAnswer__RegexpAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getStringAnswerAccess().getRegexpAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringAnswer__Group__1__Impl"


    // $ANTLR start "rule__BooleanAnswer__Group__0"
    // InternalXquiz.g:1477:1: rule__BooleanAnswer__Group__0 : rule__BooleanAnswer__Group__0__Impl rule__BooleanAnswer__Group__1 ;
    public final void rule__BooleanAnswer__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1481:1: ( rule__BooleanAnswer__Group__0__Impl rule__BooleanAnswer__Group__1 )
            // InternalXquiz.g:1482:2: rule__BooleanAnswer__Group__0__Impl rule__BooleanAnswer__Group__1
            {
            pushFollow(FOLLOW_18);
            rule__BooleanAnswer__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BooleanAnswer__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanAnswer__Group__0"


    // $ANTLR start "rule__BooleanAnswer__Group__0__Impl"
    // InternalXquiz.g:1489:1: rule__BooleanAnswer__Group__0__Impl : ( () ) ;
    public final void rule__BooleanAnswer__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1493:1: ( ( () ) )
            // InternalXquiz.g:1494:1: ( () )
            {
            // InternalXquiz.g:1494:1: ( () )
            // InternalXquiz.g:1495:2: ()
            {
             before(grammarAccess.getBooleanAnswerAccess().getBooleanAnswerAction_0()); 
            // InternalXquiz.g:1496:2: ()
            // InternalXquiz.g:1496:3: 
            {
            }

             after(grammarAccess.getBooleanAnswerAccess().getBooleanAnswerAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanAnswer__Group__0__Impl"


    // $ANTLR start "rule__BooleanAnswer__Group__1"
    // InternalXquiz.g:1504:1: rule__BooleanAnswer__Group__1 : rule__BooleanAnswer__Group__1__Impl ;
    public final void rule__BooleanAnswer__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1508:1: ( rule__BooleanAnswer__Group__1__Impl )
            // InternalXquiz.g:1509:2: rule__BooleanAnswer__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BooleanAnswer__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanAnswer__Group__1"


    // $ANTLR start "rule__BooleanAnswer__Group__1__Impl"
    // InternalXquiz.g:1515:1: rule__BooleanAnswer__Group__1__Impl : ( ( rule__BooleanAnswer__Alternatives_1 ) ) ;
    public final void rule__BooleanAnswer__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1519:1: ( ( ( rule__BooleanAnswer__Alternatives_1 ) ) )
            // InternalXquiz.g:1520:1: ( ( rule__BooleanAnswer__Alternatives_1 ) )
            {
            // InternalXquiz.g:1520:1: ( ( rule__BooleanAnswer__Alternatives_1 ) )
            // InternalXquiz.g:1521:2: ( rule__BooleanAnswer__Alternatives_1 )
            {
             before(grammarAccess.getBooleanAnswerAccess().getAlternatives_1()); 
            // InternalXquiz.g:1522:2: ( rule__BooleanAnswer__Alternatives_1 )
            // InternalXquiz.g:1522:3: rule__BooleanAnswer__Alternatives_1
            {
            pushFollow(FOLLOW_2);
            rule__BooleanAnswer__Alternatives_1();

            state._fsp--;


            }

             after(grammarAccess.getBooleanAnswerAccess().getAlternatives_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanAnswer__Group__1__Impl"


    // $ANTLR start "rule__SingleListOption__Group__0"
    // InternalXquiz.g:1531:1: rule__SingleListOption__Group__0 : rule__SingleListOption__Group__0__Impl rule__SingleListOption__Group__1 ;
    public final void rule__SingleListOption__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1535:1: ( rule__SingleListOption__Group__0__Impl rule__SingleListOption__Group__1 )
            // InternalXquiz.g:1536:2: rule__SingleListOption__Group__0__Impl rule__SingleListOption__Group__1
            {
            pushFollow(FOLLOW_19);
            rule__SingleListOption__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SingleListOption__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SingleListOption__Group__0"


    // $ANTLR start "rule__SingleListOption__Group__0__Impl"
    // InternalXquiz.g:1543:1: rule__SingleListOption__Group__0__Impl : ( '(' ) ;
    public final void rule__SingleListOption__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1547:1: ( ( '(' ) )
            // InternalXquiz.g:1548:1: ( '(' )
            {
            // InternalXquiz.g:1548:1: ( '(' )
            // InternalXquiz.g:1549:2: '('
            {
             before(grammarAccess.getSingleListOptionAccess().getLeftParenthesisKeyword_0()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getSingleListOptionAccess().getLeftParenthesisKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SingleListOption__Group__0__Impl"


    // $ANTLR start "rule__SingleListOption__Group__1"
    // InternalXquiz.g:1558:1: rule__SingleListOption__Group__1 : rule__SingleListOption__Group__1__Impl rule__SingleListOption__Group__2 ;
    public final void rule__SingleListOption__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1562:1: ( rule__SingleListOption__Group__1__Impl rule__SingleListOption__Group__2 )
            // InternalXquiz.g:1563:2: rule__SingleListOption__Group__1__Impl rule__SingleListOption__Group__2
            {
            pushFollow(FOLLOW_19);
            rule__SingleListOption__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SingleListOption__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SingleListOption__Group__1"


    // $ANTLR start "rule__SingleListOption__Group__1__Impl"
    // InternalXquiz.g:1570:1: rule__SingleListOption__Group__1__Impl : ( ( rule__SingleListOption__CorrectAssignment_1 )? ) ;
    public final void rule__SingleListOption__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1574:1: ( ( ( rule__SingleListOption__CorrectAssignment_1 )? ) )
            // InternalXquiz.g:1575:1: ( ( rule__SingleListOption__CorrectAssignment_1 )? )
            {
            // InternalXquiz.g:1575:1: ( ( rule__SingleListOption__CorrectAssignment_1 )? )
            // InternalXquiz.g:1576:2: ( rule__SingleListOption__CorrectAssignment_1 )?
            {
             before(grammarAccess.getSingleListOptionAccess().getCorrectAssignment_1()); 
            // InternalXquiz.g:1577:2: ( rule__SingleListOption__CorrectAssignment_1 )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==23) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalXquiz.g:1577:3: rule__SingleListOption__CorrectAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__SingleListOption__CorrectAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getSingleListOptionAccess().getCorrectAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SingleListOption__Group__1__Impl"


    // $ANTLR start "rule__SingleListOption__Group__2"
    // InternalXquiz.g:1585:1: rule__SingleListOption__Group__2 : rule__SingleListOption__Group__2__Impl rule__SingleListOption__Group__3 ;
    public final void rule__SingleListOption__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1589:1: ( rule__SingleListOption__Group__2__Impl rule__SingleListOption__Group__3 )
            // InternalXquiz.g:1590:2: rule__SingleListOption__Group__2__Impl rule__SingleListOption__Group__3
            {
            pushFollow(FOLLOW_18);
            rule__SingleListOption__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SingleListOption__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SingleListOption__Group__2"


    // $ANTLR start "rule__SingleListOption__Group__2__Impl"
    // InternalXquiz.g:1597:1: rule__SingleListOption__Group__2__Impl : ( ')' ) ;
    public final void rule__SingleListOption__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1601:1: ( ( ')' ) )
            // InternalXquiz.g:1602:1: ( ')' )
            {
            // InternalXquiz.g:1602:1: ( ')' )
            // InternalXquiz.g:1603:2: ')'
            {
             before(grammarAccess.getSingleListOptionAccess().getRightParenthesisKeyword_2()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getSingleListOptionAccess().getRightParenthesisKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SingleListOption__Group__2__Impl"


    // $ANTLR start "rule__SingleListOption__Group__3"
    // InternalXquiz.g:1612:1: rule__SingleListOption__Group__3 : rule__SingleListOption__Group__3__Impl ;
    public final void rule__SingleListOption__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1616:1: ( rule__SingleListOption__Group__3__Impl )
            // InternalXquiz.g:1617:2: rule__SingleListOption__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SingleListOption__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SingleListOption__Group__3"


    // $ANTLR start "rule__SingleListOption__Group__3__Impl"
    // InternalXquiz.g:1623:1: rule__SingleListOption__Group__3__Impl : ( ( rule__SingleListOption__OptionAssignment_3 ) ) ;
    public final void rule__SingleListOption__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1627:1: ( ( ( rule__SingleListOption__OptionAssignment_3 ) ) )
            // InternalXquiz.g:1628:1: ( ( rule__SingleListOption__OptionAssignment_3 ) )
            {
            // InternalXquiz.g:1628:1: ( ( rule__SingleListOption__OptionAssignment_3 ) )
            // InternalXquiz.g:1629:2: ( rule__SingleListOption__OptionAssignment_3 )
            {
             before(grammarAccess.getSingleListOptionAccess().getOptionAssignment_3()); 
            // InternalXquiz.g:1630:2: ( rule__SingleListOption__OptionAssignment_3 )
            // InternalXquiz.g:1630:3: rule__SingleListOption__OptionAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__SingleListOption__OptionAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getSingleListOptionAccess().getOptionAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SingleListOption__Group__3__Impl"


    // $ANTLR start "rule__ManyOption__Group__0"
    // InternalXquiz.g:1639:1: rule__ManyOption__Group__0 : rule__ManyOption__Group__0__Impl rule__ManyOption__Group__1 ;
    public final void rule__ManyOption__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1643:1: ( rule__ManyOption__Group__0__Impl rule__ManyOption__Group__1 )
            // InternalXquiz.g:1644:2: rule__ManyOption__Group__0__Impl rule__ManyOption__Group__1
            {
            pushFollow(FOLLOW_20);
            rule__ManyOption__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ManyOption__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyOption__Group__0"


    // $ANTLR start "rule__ManyOption__Group__0__Impl"
    // InternalXquiz.g:1651:1: rule__ManyOption__Group__0__Impl : ( '[' ) ;
    public final void rule__ManyOption__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1655:1: ( ( '[' ) )
            // InternalXquiz.g:1656:1: ( '[' )
            {
            // InternalXquiz.g:1656:1: ( '[' )
            // InternalXquiz.g:1657:2: '['
            {
             before(grammarAccess.getManyOptionAccess().getLeftSquareBracketKeyword_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getManyOptionAccess().getLeftSquareBracketKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyOption__Group__0__Impl"


    // $ANTLR start "rule__ManyOption__Group__1"
    // InternalXquiz.g:1666:1: rule__ManyOption__Group__1 : rule__ManyOption__Group__1__Impl rule__ManyOption__Group__2 ;
    public final void rule__ManyOption__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1670:1: ( rule__ManyOption__Group__1__Impl rule__ManyOption__Group__2 )
            // InternalXquiz.g:1671:2: rule__ManyOption__Group__1__Impl rule__ManyOption__Group__2
            {
            pushFollow(FOLLOW_20);
            rule__ManyOption__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ManyOption__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyOption__Group__1"


    // $ANTLR start "rule__ManyOption__Group__1__Impl"
    // InternalXquiz.g:1678:1: rule__ManyOption__Group__1__Impl : ( ( rule__ManyOption__CorrectAssignment_1 )? ) ;
    public final void rule__ManyOption__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1682:1: ( ( ( rule__ManyOption__CorrectAssignment_1 )? ) )
            // InternalXquiz.g:1683:1: ( ( rule__ManyOption__CorrectAssignment_1 )? )
            {
            // InternalXquiz.g:1683:1: ( ( rule__ManyOption__CorrectAssignment_1 )? )
            // InternalXquiz.g:1684:2: ( rule__ManyOption__CorrectAssignment_1 )?
            {
             before(grammarAccess.getManyOptionAccess().getCorrectAssignment_1()); 
            // InternalXquiz.g:1685:2: ( rule__ManyOption__CorrectAssignment_1 )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==23) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalXquiz.g:1685:3: rule__ManyOption__CorrectAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__ManyOption__CorrectAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getManyOptionAccess().getCorrectAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyOption__Group__1__Impl"


    // $ANTLR start "rule__ManyOption__Group__2"
    // InternalXquiz.g:1693:1: rule__ManyOption__Group__2 : rule__ManyOption__Group__2__Impl rule__ManyOption__Group__3 ;
    public final void rule__ManyOption__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1697:1: ( rule__ManyOption__Group__2__Impl rule__ManyOption__Group__3 )
            // InternalXquiz.g:1698:2: rule__ManyOption__Group__2__Impl rule__ManyOption__Group__3
            {
            pushFollow(FOLLOW_18);
            rule__ManyOption__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ManyOption__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyOption__Group__2"


    // $ANTLR start "rule__ManyOption__Group__2__Impl"
    // InternalXquiz.g:1705:1: rule__ManyOption__Group__2__Impl : ( ']' ) ;
    public final void rule__ManyOption__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1709:1: ( ( ']' ) )
            // InternalXquiz.g:1710:1: ( ']' )
            {
            // InternalXquiz.g:1710:1: ( ']' )
            // InternalXquiz.g:1711:2: ']'
            {
             before(grammarAccess.getManyOptionAccess().getRightSquareBracketKeyword_2()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getManyOptionAccess().getRightSquareBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyOption__Group__2__Impl"


    // $ANTLR start "rule__ManyOption__Group__3"
    // InternalXquiz.g:1720:1: rule__ManyOption__Group__3 : rule__ManyOption__Group__3__Impl ;
    public final void rule__ManyOption__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1724:1: ( rule__ManyOption__Group__3__Impl )
            // InternalXquiz.g:1725:2: rule__ManyOption__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ManyOption__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyOption__Group__3"


    // $ANTLR start "rule__ManyOption__Group__3__Impl"
    // InternalXquiz.g:1731:1: rule__ManyOption__Group__3__Impl : ( ( rule__ManyOption__OptionAssignment_3 ) ) ;
    public final void rule__ManyOption__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1735:1: ( ( ( rule__ManyOption__OptionAssignment_3 ) ) )
            // InternalXquiz.g:1736:1: ( ( rule__ManyOption__OptionAssignment_3 ) )
            {
            // InternalXquiz.g:1736:1: ( ( rule__ManyOption__OptionAssignment_3 ) )
            // InternalXquiz.g:1737:2: ( rule__ManyOption__OptionAssignment_3 )
            {
             before(grammarAccess.getManyOptionAccess().getOptionAssignment_3()); 
            // InternalXquiz.g:1738:2: ( rule__ManyOption__OptionAssignment_3 )
            // InternalXquiz.g:1738:3: rule__ManyOption__OptionAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__ManyOption__OptionAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getManyOptionAccess().getOptionAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyOption__Group__3__Impl"


    // $ANTLR start "rule__Quiz__NameAssignment_1"
    // InternalXquiz.g:1747:1: rule__Quiz__NameAssignment_1 : ( ruleQName ) ;
    public final void rule__Quiz__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1751:1: ( ( ruleQName ) )
            // InternalXquiz.g:1752:2: ( ruleQName )
            {
            // InternalXquiz.g:1752:2: ( ruleQName )
            // InternalXquiz.g:1753:3: ruleQName
            {
             before(grammarAccess.getQuizAccess().getNameQNameParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleQName();

            state._fsp--;

             after(grammarAccess.getQuizAccess().getNameQNameParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Quiz__NameAssignment_1"


    // $ANTLR start "rule__Quiz__TitleAssignment_2"
    // InternalXquiz.g:1762:1: rule__Quiz__TitleAssignment_2 : ( RULE_STRING ) ;
    public final void rule__Quiz__TitleAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1766:1: ( ( RULE_STRING ) )
            // InternalXquiz.g:1767:2: ( RULE_STRING )
            {
            // InternalXquiz.g:1767:2: ( RULE_STRING )
            // InternalXquiz.g:1768:3: RULE_STRING
            {
             before(grammarAccess.getQuizAccess().getTitleSTRINGTerminalRuleCall_2_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getQuizAccess().getTitleSTRINGTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Quiz__TitleAssignment_2"


    // $ANTLR start "rule__Quiz__PartsAssignment_3"
    // InternalXquiz.g:1777:1: rule__Quiz__PartsAssignment_3 : ( ruleAbstractQuizPart ) ;
    public final void rule__Quiz__PartsAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1781:1: ( ( ruleAbstractQuizPart ) )
            // InternalXquiz.g:1782:2: ( ruleAbstractQuizPart )
            {
            // InternalXquiz.g:1782:2: ( ruleAbstractQuizPart )
            // InternalXquiz.g:1783:3: ruleAbstractQuizPart
            {
             before(grammarAccess.getQuizAccess().getPartsAbstractQuizPartParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleAbstractQuizPart();

            state._fsp--;

             after(grammarAccess.getQuizAccess().getPartsAbstractQuizPartParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Quiz__PartsAssignment_3"


    // $ANTLR start "rule__QuizPartRef__PartRefAssignment_2"
    // InternalXquiz.g:1792:1: rule__QuizPartRef__PartRefAssignment_2 : ( ( ruleQName ) ) ;
    public final void rule__QuizPartRef__PartRefAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1796:1: ( ( ( ruleQName ) ) )
            // InternalXquiz.g:1797:2: ( ( ruleQName ) )
            {
            // InternalXquiz.g:1797:2: ( ( ruleQName ) )
            // InternalXquiz.g:1798:3: ( ruleQName )
            {
             before(grammarAccess.getQuizPartRefAccess().getPartRefQuizPartCrossReference_2_0()); 
            // InternalXquiz.g:1799:3: ( ruleQName )
            // InternalXquiz.g:1800:4: ruleQName
            {
             before(grammarAccess.getQuizPartRefAccess().getPartRefQuizPartQNameParserRuleCall_2_0_1()); 
            pushFollow(FOLLOW_2);
            ruleQName();

            state._fsp--;

             after(grammarAccess.getQuizPartRefAccess().getPartRefQuizPartQNameParserRuleCall_2_0_1()); 

            }

             after(grammarAccess.getQuizPartRefAccess().getPartRefQuizPartCrossReference_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuizPartRef__PartRefAssignment_2"


    // $ANTLR start "rule__QuizPart__NameAssignment_1"
    // InternalXquiz.g:1811:1: rule__QuizPart__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__QuizPart__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1815:1: ( ( RULE_ID ) )
            // InternalXquiz.g:1816:2: ( RULE_ID )
            {
            // InternalXquiz.g:1816:2: ( RULE_ID )
            // InternalXquiz.g:1817:3: RULE_ID
            {
             before(grammarAccess.getQuizPartAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getQuizPartAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuizPart__NameAssignment_1"


    // $ANTLR start "rule__QuizPart__TitleAssignment_2"
    // InternalXquiz.g:1826:1: rule__QuizPart__TitleAssignment_2 : ( RULE_STRING ) ;
    public final void rule__QuizPart__TitleAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1830:1: ( ( RULE_STRING ) )
            // InternalXquiz.g:1831:2: ( RULE_STRING )
            {
            // InternalXquiz.g:1831:2: ( RULE_STRING )
            // InternalXquiz.g:1832:3: RULE_STRING
            {
             before(grammarAccess.getQuizPartAccess().getTitleSTRINGTerminalRuleCall_2_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getQuizPartAccess().getTitleSTRINGTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuizPart__TitleAssignment_2"


    // $ANTLR start "rule__QuizPart__QuestionsAssignment_3"
    // InternalXquiz.g:1841:1: rule__QuizPart__QuestionsAssignment_3 : ( ruleAbstractQA ) ;
    public final void rule__QuizPart__QuestionsAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1845:1: ( ( ruleAbstractQA ) )
            // InternalXquiz.g:1846:2: ( ruleAbstractQA )
            {
            // InternalXquiz.g:1846:2: ( ruleAbstractQA )
            // InternalXquiz.g:1847:3: ruleAbstractQA
            {
             before(grammarAccess.getQuizPartAccess().getQuestionsAbstractQAParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleAbstractQA();

            state._fsp--;

             after(grammarAccess.getQuizPartAccess().getQuestionsAbstractQAParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuizPart__QuestionsAssignment_3"


    // $ANTLR start "rule__QARef__QaRefAssignment_1"
    // InternalXquiz.g:1856:1: rule__QARef__QaRefAssignment_1 : ( ( ruleQName ) ) ;
    public final void rule__QARef__QaRefAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1860:1: ( ( ( ruleQName ) ) )
            // InternalXquiz.g:1861:2: ( ( ruleQName ) )
            {
            // InternalXquiz.g:1861:2: ( ( ruleQName ) )
            // InternalXquiz.g:1862:3: ( ruleQName )
            {
             before(grammarAccess.getQARefAccess().getQaRefQACrossReference_1_0()); 
            // InternalXquiz.g:1863:3: ( ruleQName )
            // InternalXquiz.g:1864:4: ruleQName
            {
             before(grammarAccess.getQARefAccess().getQaRefQAQNameParserRuleCall_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleQName();

            state._fsp--;

             after(grammarAccess.getQARefAccess().getQaRefQAQNameParserRuleCall_1_0_1()); 

            }

             after(grammarAccess.getQARefAccess().getQaRefQACrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QARef__QaRefAssignment_1"


    // $ANTLR start "rule__QA__NameAssignment_0"
    // InternalXquiz.g:1875:1: rule__QA__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__QA__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1879:1: ( ( RULE_ID ) )
            // InternalXquiz.g:1880:2: ( RULE_ID )
            {
            // InternalXquiz.g:1880:2: ( RULE_ID )
            // InternalXquiz.g:1881:3: RULE_ID
            {
             before(grammarAccess.getQAAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getQAAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QA__NameAssignment_0"


    // $ANTLR start "rule__QA__QAssignment_1"
    // InternalXquiz.g:1890:1: rule__QA__QAssignment_1 : ( ruleQuestion ) ;
    public final void rule__QA__QAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1894:1: ( ( ruleQuestion ) )
            // InternalXquiz.g:1895:2: ( ruleQuestion )
            {
            // InternalXquiz.g:1895:2: ( ruleQuestion )
            // InternalXquiz.g:1896:3: ruleQuestion
            {
             before(grammarAccess.getQAAccess().getQQuestionParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleQuestion();

            state._fsp--;

             after(grammarAccess.getQAAccess().getQQuestionParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QA__QAssignment_1"


    // $ANTLR start "rule__QA__AAssignment_3"
    // InternalXquiz.g:1905:1: rule__QA__AAssignment_3 : ( ruleAnswer ) ;
    public final void rule__QA__AAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1909:1: ( ( ruleAnswer ) )
            // InternalXquiz.g:1910:2: ( ruleAnswer )
            {
            // InternalXquiz.g:1910:2: ( ruleAnswer )
            // InternalXquiz.g:1911:3: ruleAnswer
            {
             before(grammarAccess.getQAAccess().getAAnswerParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleAnswer();

            state._fsp--;

             after(grammarAccess.getQAAccess().getAAnswerParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QA__AAssignment_3"


    // $ANTLR start "rule__StringQuestion__QuestionAssignment"
    // InternalXquiz.g:1920:1: rule__StringQuestion__QuestionAssignment : ( RULE_STRING ) ;
    public final void rule__StringQuestion__QuestionAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1924:1: ( ( RULE_STRING ) )
            // InternalXquiz.g:1925:2: ( RULE_STRING )
            {
            // InternalXquiz.g:1925:2: ( RULE_STRING )
            // InternalXquiz.g:1926:3: RULE_STRING
            {
             before(grammarAccess.getStringQuestionAccess().getQuestionSTRINGTerminalRuleCall_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getStringQuestionAccess().getQuestionSTRINGTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringQuestion__QuestionAssignment"


    // $ANTLR start "rule__StringAnswer__ValueAssignment_0"
    // InternalXquiz.g:1935:1: rule__StringAnswer__ValueAssignment_0 : ( RULE_STRING ) ;
    public final void rule__StringAnswer__ValueAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1939:1: ( ( RULE_STRING ) )
            // InternalXquiz.g:1940:2: ( RULE_STRING )
            {
            // InternalXquiz.g:1940:2: ( RULE_STRING )
            // InternalXquiz.g:1941:3: RULE_STRING
            {
             before(grammarAccess.getStringAnswerAccess().getValueSTRINGTerminalRuleCall_0_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getStringAnswerAccess().getValueSTRINGTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringAnswer__ValueAssignment_0"


    // $ANTLR start "rule__StringAnswer__RegexpAssignment_1"
    // InternalXquiz.g:1950:1: rule__StringAnswer__RegexpAssignment_1 : ( ( '~' ) ) ;
    public final void rule__StringAnswer__RegexpAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1954:1: ( ( ( '~' ) ) )
            // InternalXquiz.g:1955:2: ( ( '~' ) )
            {
            // InternalXquiz.g:1955:2: ( ( '~' ) )
            // InternalXquiz.g:1956:3: ( '~' )
            {
             before(grammarAccess.getStringAnswerAccess().getRegexpTildeKeyword_1_0()); 
            // InternalXquiz.g:1957:3: ( '~' )
            // InternalXquiz.g:1958:4: '~'
            {
             before(grammarAccess.getStringAnswerAccess().getRegexpTildeKeyword_1_0()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getStringAnswerAccess().getRegexpTildeKeyword_1_0()); 

            }

             after(grammarAccess.getStringAnswerAccess().getRegexpTildeKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringAnswer__RegexpAssignment_1"


    // $ANTLR start "rule__NumberAnswer__ValueAssignment"
    // InternalXquiz.g:1969:1: rule__NumberAnswer__ValueAssignment : ( ruleEDouble ) ;
    public final void rule__NumberAnswer__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1973:1: ( ( ruleEDouble ) )
            // InternalXquiz.g:1974:2: ( ruleEDouble )
            {
            // InternalXquiz.g:1974:2: ( ruleEDouble )
            // InternalXquiz.g:1975:3: ruleEDouble
            {
             before(grammarAccess.getNumberAnswerAccess().getValueEDoubleParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleEDouble();

            state._fsp--;

             after(grammarAccess.getNumberAnswerAccess().getValueEDoubleParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberAnswer__ValueAssignment"


    // $ANTLR start "rule__BooleanAnswer__ValueAssignment_1_0"
    // InternalXquiz.g:1984:1: rule__BooleanAnswer__ValueAssignment_1_0 : ( ( 'yes' ) ) ;
    public final void rule__BooleanAnswer__ValueAssignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:1988:1: ( ( ( 'yes' ) ) )
            // InternalXquiz.g:1989:2: ( ( 'yes' ) )
            {
            // InternalXquiz.g:1989:2: ( ( 'yes' ) )
            // InternalXquiz.g:1990:3: ( 'yes' )
            {
             before(grammarAccess.getBooleanAnswerAccess().getValueYesKeyword_1_0_0()); 
            // InternalXquiz.g:1991:3: ( 'yes' )
            // InternalXquiz.g:1992:4: 'yes'
            {
             before(grammarAccess.getBooleanAnswerAccess().getValueYesKeyword_1_0_0()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getBooleanAnswerAccess().getValueYesKeyword_1_0_0()); 

            }

             after(grammarAccess.getBooleanAnswerAccess().getValueYesKeyword_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanAnswer__ValueAssignment_1_0"


    // $ANTLR start "rule__SingleOptionsAnswer__OptionsAssignment"
    // InternalXquiz.g:2003:1: rule__SingleOptionsAnswer__OptionsAssignment : ( ruleSingleListOption ) ;
    public final void rule__SingleOptionsAnswer__OptionsAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:2007:1: ( ( ruleSingleListOption ) )
            // InternalXquiz.g:2008:2: ( ruleSingleListOption )
            {
            // InternalXquiz.g:2008:2: ( ruleSingleListOption )
            // InternalXquiz.g:2009:3: ruleSingleListOption
            {
             before(grammarAccess.getSingleOptionsAnswerAccess().getOptionsSingleListOptionParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleSingleListOption();

            state._fsp--;

             after(grammarAccess.getSingleOptionsAnswerAccess().getOptionsSingleListOptionParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SingleOptionsAnswer__OptionsAssignment"


    // $ANTLR start "rule__SingleListOption__CorrectAssignment_1"
    // InternalXquiz.g:2018:1: rule__SingleListOption__CorrectAssignment_1 : ( ( 'x' ) ) ;
    public final void rule__SingleListOption__CorrectAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:2022:1: ( ( ( 'x' ) ) )
            // InternalXquiz.g:2023:2: ( ( 'x' ) )
            {
            // InternalXquiz.g:2023:2: ( ( 'x' ) )
            // InternalXquiz.g:2024:3: ( 'x' )
            {
             before(grammarAccess.getSingleListOptionAccess().getCorrectXKeyword_1_0()); 
            // InternalXquiz.g:2025:3: ( 'x' )
            // InternalXquiz.g:2026:4: 'x'
            {
             before(grammarAccess.getSingleListOptionAccess().getCorrectXKeyword_1_0()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getSingleListOptionAccess().getCorrectXKeyword_1_0()); 

            }

             after(grammarAccess.getSingleListOptionAccess().getCorrectXKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SingleListOption__CorrectAssignment_1"


    // $ANTLR start "rule__SingleListOption__OptionAssignment_3"
    // InternalXquiz.g:2037:1: rule__SingleListOption__OptionAssignment_3 : ( ruleOptionAnswer ) ;
    public final void rule__SingleListOption__OptionAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:2041:1: ( ( ruleOptionAnswer ) )
            // InternalXquiz.g:2042:2: ( ruleOptionAnswer )
            {
            // InternalXquiz.g:2042:2: ( ruleOptionAnswer )
            // InternalXquiz.g:2043:3: ruleOptionAnswer
            {
             before(grammarAccess.getSingleListOptionAccess().getOptionOptionAnswerParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleOptionAnswer();

            state._fsp--;

             after(grammarAccess.getSingleListOptionAccess().getOptionOptionAnswerParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SingleListOption__OptionAssignment_3"


    // $ANTLR start "rule__ManyOptionsAnswer__OptionsAssignment"
    // InternalXquiz.g:2052:1: rule__ManyOptionsAnswer__OptionsAssignment : ( ruleManyOption ) ;
    public final void rule__ManyOptionsAnswer__OptionsAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:2056:1: ( ( ruleManyOption ) )
            // InternalXquiz.g:2057:2: ( ruleManyOption )
            {
            // InternalXquiz.g:2057:2: ( ruleManyOption )
            // InternalXquiz.g:2058:3: ruleManyOption
            {
             before(grammarAccess.getManyOptionsAnswerAccess().getOptionsManyOptionParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleManyOption();

            state._fsp--;

             after(grammarAccess.getManyOptionsAnswerAccess().getOptionsManyOptionParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyOptionsAnswer__OptionsAssignment"


    // $ANTLR start "rule__ManyOption__CorrectAssignment_1"
    // InternalXquiz.g:2067:1: rule__ManyOption__CorrectAssignment_1 : ( ( 'x' ) ) ;
    public final void rule__ManyOption__CorrectAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:2071:1: ( ( ( 'x' ) ) )
            // InternalXquiz.g:2072:2: ( ( 'x' ) )
            {
            // InternalXquiz.g:2072:2: ( ( 'x' ) )
            // InternalXquiz.g:2073:3: ( 'x' )
            {
             before(grammarAccess.getManyOptionAccess().getCorrectXKeyword_1_0()); 
            // InternalXquiz.g:2074:3: ( 'x' )
            // InternalXquiz.g:2075:4: 'x'
            {
             before(grammarAccess.getManyOptionAccess().getCorrectXKeyword_1_0()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getManyOptionAccess().getCorrectXKeyword_1_0()); 

            }

             after(grammarAccess.getManyOptionAccess().getCorrectXKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyOption__CorrectAssignment_1"


    // $ANTLR start "rule__ManyOption__OptionAssignment_3"
    // InternalXquiz.g:2086:1: rule__ManyOption__OptionAssignment_3 : ( ruleOptionAnswer ) ;
    public final void rule__ManyOption__OptionAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXquiz.g:2090:1: ( ( ruleOptionAnswer ) )
            // InternalXquiz.g:2091:2: ( ruleOptionAnswer )
            {
            // InternalXquiz.g:2091:2: ( ruleOptionAnswer )
            // InternalXquiz.g:2092:3: ruleOptionAnswer
            {
             before(grammarAccess.getManyOptionAccess().getOptionOptionAnswerParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleOptionAnswer();

            state._fsp--;

             after(grammarAccess.getManyOptionAccess().getOptionOptionAnswerParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ManyOption__OptionAssignment_3"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000004040L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000004002L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000002002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000008050L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000008052L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000000050L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x00000000004B0860L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000400860L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000840000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000900000L});

}