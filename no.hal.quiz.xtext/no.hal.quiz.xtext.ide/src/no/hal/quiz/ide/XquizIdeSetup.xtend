/*
 * generated by Xtext 2.14.0
 */
package no.hal.quiz.ide

import com.google.inject.Guice
import no.hal.quiz.XquizRuntimeModule
import no.hal.quiz.XquizStandaloneSetup
import org.eclipse.xtext.util.Modules2

/**
 * Initialization support for running Xtext languages as language servers.
 */
class XquizIdeSetup extends XquizStandaloneSetup {

	override createInjector() {
		Guice.createInjector(Modules2.mixin(new XquizRuntimeModule, new XquizIdeModule))
	}
	
}
