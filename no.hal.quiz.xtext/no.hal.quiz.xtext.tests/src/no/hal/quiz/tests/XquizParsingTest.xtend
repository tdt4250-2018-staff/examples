/*
 * generated by Xtext 2.14.0
 */
package no.hal.quiz.tests

import com.google.inject.Inject
import no.hal.quiz.Quiz
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.XtextRunner
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import no.hal.quiz.QuizPart

@RunWith(XtextRunner)
@InjectWith(XquizInjectorProvider)
class XquizParsingTest {

	@Inject
	ParseHelper<Quiz> parseHelper
	
	@Test
	def void parseQuiz() {
		val result = parseHelper.parse('''
			quiz myQuiz "My first quiz (test)"
		''')
		Assert.assertNotNull(result)
		val errors = result.eResource.errors
		Assert.assertTrue('''Unexpected errors: «errors.join(", ")»''', errors.isEmpty)
		Assert.assertEquals("myQuiz", result.name)
		Assert.assertEquals("My first quiz (test)", result.title)
	}

	@Test
	def void parseQuizWithParts() {
		val result = parseHelper.parse('''
			quiz quiz1 "..."
			part part1 "Part 1"
			part part2 "Part 2"
		''')
		Assert.assertNotNull(result)
		val errors = result.eResource.errors
		Assert.assertTrue('''Unexpected errors: «errors.join(", ")»''', errors.isEmpty)
		Assert.assertEquals(2, result.parts.size)
		
		checkNameAndTitle(result.parts.get(0) as QuizPart, "part1", "Part 1")
		checkNameAndTitle(result.parts.get(1) as QuizPart, "part2", "Part 2")
	}
	
	def void checkNameAndTitle(QuizPart part, String name, String title) {
		Assert.assertEquals(name, part.name)
		Assert.assertEquals(title, part.title)		
	}
}
