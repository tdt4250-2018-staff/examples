/*
 * generated by Xtext 2.14.0
 */
package no.hal.quiz.serializer;

import com.google.inject.Inject;
import java.util.Set;
import no.hal.quiz.BooleanAnswer;
import no.hal.quiz.ManyOptionsAnswer;
import no.hal.quiz.NumberAnswer;
import no.hal.quiz.Option;
import no.hal.quiz.QA;
import no.hal.quiz.QARef;
import no.hal.quiz.Quiz;
import no.hal.quiz.QuizPackage;
import no.hal.quiz.QuizPart;
import no.hal.quiz.QuizPartRef;
import no.hal.quiz.SingleOptionsAnswer;
import no.hal.quiz.StringAnswer;
import no.hal.quiz.StringQuestion;
import no.hal.quiz.services.XquizGrammarAccess;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.xtext.Action;
import org.eclipse.xtext.Parameter;
import org.eclipse.xtext.ParserRule;
import org.eclipse.xtext.serializer.ISerializationContext;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService.ValueTransient;

@SuppressWarnings("all")
public class XquizSemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private XquizGrammarAccess grammarAccess;
	
	@Override
	public void sequence(ISerializationContext context, EObject semanticObject) {
		EPackage epackage = semanticObject.eClass().getEPackage();
		ParserRule rule = context.getParserRule();
		Action action = context.getAssignedAction();
		Set<Parameter> parameters = context.getEnabledBooleanParameters();
		if (epackage == QuizPackage.eINSTANCE)
			switch (semanticObject.eClass().getClassifierID()) {
			case QuizPackage.BOOLEAN_ANSWER:
				sequence_BooleanAnswer(context, (BooleanAnswer) semanticObject); 
				return; 
			case QuizPackage.MANY_OPTIONS_ANSWER:
				sequence_ManyOptionsAnswer(context, (ManyOptionsAnswer) semanticObject); 
				return; 
			case QuizPackage.NUMBER_ANSWER:
				sequence_NumberAnswer(context, (NumberAnswer) semanticObject); 
				return; 
			case QuizPackage.OPTION:
				if (rule == grammarAccess.getManyOptionRule()) {
					sequence_ManyOption(context, (Option) semanticObject); 
					return; 
				}
				else if (rule == grammarAccess.getOptionRule()) {
					sequence_ManyOption_SingleListOption(context, (Option) semanticObject); 
					return; 
				}
				else if (rule == grammarAccess.getSingleListOptionRule()) {
					sequence_SingleListOption(context, (Option) semanticObject); 
					return; 
				}
				else break;
			case QuizPackage.QA:
				sequence_QA(context, (QA) semanticObject); 
				return; 
			case QuizPackage.QA_REF:
				sequence_QARef(context, (QARef) semanticObject); 
				return; 
			case QuizPackage.QUIZ:
				sequence_Quiz(context, (Quiz) semanticObject); 
				return; 
			case QuizPackage.QUIZ_PART:
				sequence_QuizPart(context, (QuizPart) semanticObject); 
				return; 
			case QuizPackage.QUIZ_PART_REF:
				sequence_QuizPartRef(context, (QuizPartRef) semanticObject); 
				return; 
			case QuizPackage.SINGLE_OPTIONS_ANSWER:
				sequence_SingleOptionsAnswer(context, (SingleOptionsAnswer) semanticObject); 
				return; 
			case QuizPackage.STRING_ANSWER:
				sequence_StringAnswer(context, (StringAnswer) semanticObject); 
				return; 
			case QuizPackage.STRING_QUESTION:
				sequence_StringQuestion(context, (StringQuestion) semanticObject); 
				return; 
			}
		if (errorAcceptor != null)
			errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Contexts:
	 *     Answer returns BooleanAnswer
	 *     OptionAnswer returns BooleanAnswer
	 *     SimpleAnswer returns BooleanAnswer
	 *     BooleanAnswer returns BooleanAnswer
	 *
	 * Constraint:
	 *     value?='yes'?
	 */
	protected void sequence_BooleanAnswer(ISerializationContext context, BooleanAnswer semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     ManyOption returns Option
	 *
	 * Constraint:
	 *     (correct?='x'? option=OptionAnswer)
	 */
	protected void sequence_ManyOption(ISerializationContext context, Option semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Option returns Option
	 *
	 * Constraint:
	 *     ((correct?='x'? option=OptionAnswer) | (correct?='x'? option=OptionAnswer))
	 */
	protected void sequence_ManyOption_SingleListOption(ISerializationContext context, Option semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Answer returns ManyOptionsAnswer
	 *     OptionsAnswer returns ManyOptionsAnswer
	 *     ManyOptionsAnswer returns ManyOptionsAnswer
	 *
	 * Constraint:
	 *     options+=ManyOption+
	 */
	protected void sequence_ManyOptionsAnswer(ISerializationContext context, ManyOptionsAnswer semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Answer returns NumberAnswer
	 *     OptionAnswer returns NumberAnswer
	 *     SimpleAnswer returns NumberAnswer
	 *     NumberAnswer returns NumberAnswer
	 *
	 * Constraint:
	 *     value=EDouble
	 */
	protected void sequence_NumberAnswer(ISerializationContext context, NumberAnswer semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, QuizPackage.Literals.NUMBER_ANSWER__VALUE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QuizPackage.Literals.NUMBER_ANSWER__VALUE));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getNumberAnswerAccess().getValueEDoubleParserRuleCall_0(), semanticObject.getValue());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     AbstractQA returns QARef
	 *     QARef returns QARef
	 *
	 * Constraint:
	 *     qaRef=[QA|QName]
	 */
	protected void sequence_QARef(ISerializationContext context, QARef semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, QuizPackage.Literals.QA_REF__QA_REF) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QuizPackage.Literals.QA_REF__QA_REF));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getQARefAccess().getQaRefQAQNameParserRuleCall_1_0_1(), semanticObject.eGet(QuizPackage.Literals.QA_REF__QA_REF, false));
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     AbstractQA returns QA
	 *     QA returns QA
	 *
	 * Constraint:
	 *     (name=ID? q=Question a=Answer)
	 */
	protected void sequence_QA(ISerializationContext context, QA semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     AbstractQuizPart returns QuizPartRef
	 *     QuizPartRef returns QuizPartRef
	 *
	 * Constraint:
	 *     partRef=[QuizPart|QName]
	 */
	protected void sequence_QuizPartRef(ISerializationContext context, QuizPartRef semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, QuizPackage.Literals.QUIZ_PART_REF__PART_REF) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QuizPackage.Literals.QUIZ_PART_REF__PART_REF));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getQuizPartRefAccess().getPartRefQuizPartQNameParserRuleCall_2_0_1(), semanticObject.eGet(QuizPackage.Literals.QUIZ_PART_REF__PART_REF, false));
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     AbstractQuizPart returns QuizPart
	 *     QuizPart returns QuizPart
	 *
	 * Constraint:
	 *     (name=ID title=STRING questions+=AbstractQA*)
	 */
	protected void sequence_QuizPart(ISerializationContext context, QuizPart semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Quiz returns Quiz
	 *
	 * Constraint:
	 *     (name=QName title=STRING? parts+=AbstractQuizPart*)
	 */
	protected void sequence_Quiz(ISerializationContext context, Quiz semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     SingleListOption returns Option
	 *
	 * Constraint:
	 *     (correct?='x'? option=OptionAnswer)
	 */
	protected void sequence_SingleListOption(ISerializationContext context, Option semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Answer returns SingleOptionsAnswer
	 *     OptionsAnswer returns SingleOptionsAnswer
	 *     SingleOptionsAnswer returns SingleOptionsAnswer
	 *
	 * Constraint:
	 *     options+=SingleListOption+
	 */
	protected void sequence_SingleOptionsAnswer(ISerializationContext context, SingleOptionsAnswer semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Answer returns StringAnswer
	 *     OptionAnswer returns StringAnswer
	 *     SimpleAnswer returns StringAnswer
	 *     StringAnswer returns StringAnswer
	 *
	 * Constraint:
	 *     (value=STRING regexp?='~'?)
	 */
	protected void sequence_StringAnswer(ISerializationContext context, StringAnswer semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Question returns StringQuestion
	 *     StringQuestion returns StringQuestion
	 *
	 * Constraint:
	 *     question=STRING
	 */
	protected void sequence_StringQuestion(ISerializationContext context, StringQuestion semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, QuizPackage.Literals.STRING_QUESTION__QUESTION) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, QuizPackage.Literals.STRING_QUESTION__QUESTION));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getStringQuestionAccess().getQuestionSTRINGTerminalRuleCall_0(), semanticObject.getQuestion());
		feeder.finish();
	}
	
	
}
