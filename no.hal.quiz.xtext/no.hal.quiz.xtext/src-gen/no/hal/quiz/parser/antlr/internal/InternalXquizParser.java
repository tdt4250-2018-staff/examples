package no.hal.quiz.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import no.hal.quiz.services.XquizGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalXquizParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'quiz'", "'.'", "'part'", "'@'", "'?'", "'~'", "'yes'", "'no'", "'('", "'x'", "')'", "'['", "']'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=6;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalXquizParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalXquizParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalXquizParser.tokenNames; }
    public String getGrammarFileName() { return "InternalXquiz.g"; }



     	private XquizGrammarAccess grammarAccess;

        public InternalXquizParser(TokenStream input, XquizGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Quiz";
       	}

       	@Override
       	protected XquizGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleQuiz"
    // InternalXquiz.g:64:1: entryRuleQuiz returns [EObject current=null] : iv_ruleQuiz= ruleQuiz EOF ;
    public final EObject entryRuleQuiz() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQuiz = null;


        try {
            // InternalXquiz.g:64:45: (iv_ruleQuiz= ruleQuiz EOF )
            // InternalXquiz.g:65:2: iv_ruleQuiz= ruleQuiz EOF
            {
             newCompositeNode(grammarAccess.getQuizRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQuiz=ruleQuiz();

            state._fsp--;

             current =iv_ruleQuiz; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQuiz"


    // $ANTLR start "ruleQuiz"
    // InternalXquiz.g:71:1: ruleQuiz returns [EObject current=null] : (otherlv_0= 'quiz' ( (lv_name_1_0= ruleQName ) ) ( (lv_title_2_0= RULE_STRING ) )? ( (lv_parts_3_0= ruleAbstractQuizPart ) )* ) ;
    public final EObject ruleQuiz() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_title_2_0=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        EObject lv_parts_3_0 = null;



        	enterRule();

        try {
            // InternalXquiz.g:77:2: ( (otherlv_0= 'quiz' ( (lv_name_1_0= ruleQName ) ) ( (lv_title_2_0= RULE_STRING ) )? ( (lv_parts_3_0= ruleAbstractQuizPart ) )* ) )
            // InternalXquiz.g:78:2: (otherlv_0= 'quiz' ( (lv_name_1_0= ruleQName ) ) ( (lv_title_2_0= RULE_STRING ) )? ( (lv_parts_3_0= ruleAbstractQuizPart ) )* )
            {
            // InternalXquiz.g:78:2: (otherlv_0= 'quiz' ( (lv_name_1_0= ruleQName ) ) ( (lv_title_2_0= RULE_STRING ) )? ( (lv_parts_3_0= ruleAbstractQuizPart ) )* )
            // InternalXquiz.g:79:3: otherlv_0= 'quiz' ( (lv_name_1_0= ruleQName ) ) ( (lv_title_2_0= RULE_STRING ) )? ( (lv_parts_3_0= ruleAbstractQuizPart ) )*
            {
            otherlv_0=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getQuizAccess().getQuizKeyword_0());
            		
            // InternalXquiz.g:83:3: ( (lv_name_1_0= ruleQName ) )
            // InternalXquiz.g:84:4: (lv_name_1_0= ruleQName )
            {
            // InternalXquiz.g:84:4: (lv_name_1_0= ruleQName )
            // InternalXquiz.g:85:5: lv_name_1_0= ruleQName
            {

            					newCompositeNode(grammarAccess.getQuizAccess().getNameQNameParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_1_0=ruleQName();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getQuizRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"no.hal.quiz.Xquiz.QName");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalXquiz.g:102:3: ( (lv_title_2_0= RULE_STRING ) )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==RULE_STRING) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // InternalXquiz.g:103:4: (lv_title_2_0= RULE_STRING )
                    {
                    // InternalXquiz.g:103:4: (lv_title_2_0= RULE_STRING )
                    // InternalXquiz.g:104:5: lv_title_2_0= RULE_STRING
                    {
                    lv_title_2_0=(Token)match(input,RULE_STRING,FOLLOW_5); 

                    					newLeafNode(lv_title_2_0, grammarAccess.getQuizAccess().getTitleSTRINGTerminalRuleCall_2_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getQuizRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"title",
                    						lv_title_2_0,
                    						"org.eclipse.xtext.common.Terminals.STRING");
                    				

                    }


                    }
                    break;

            }

            // InternalXquiz.g:120:3: ( (lv_parts_3_0= ruleAbstractQuizPart ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==13) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalXquiz.g:121:4: (lv_parts_3_0= ruleAbstractQuizPart )
            	    {
            	    // InternalXquiz.g:121:4: (lv_parts_3_0= ruleAbstractQuizPart )
            	    // InternalXquiz.g:122:5: lv_parts_3_0= ruleAbstractQuizPart
            	    {

            	    					newCompositeNode(grammarAccess.getQuizAccess().getPartsAbstractQuizPartParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_5);
            	    lv_parts_3_0=ruleAbstractQuizPart();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getQuizRule());
            	    					}
            	    					add(
            	    						current,
            	    						"parts",
            	    						lv_parts_3_0,
            	    						"no.hal.quiz.Xquiz.AbstractQuizPart");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQuiz"


    // $ANTLR start "entryRuleQName"
    // InternalXquiz.g:143:1: entryRuleQName returns [String current=null] : iv_ruleQName= ruleQName EOF ;
    public final String entryRuleQName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQName = null;


        try {
            // InternalXquiz.g:143:45: (iv_ruleQName= ruleQName EOF )
            // InternalXquiz.g:144:2: iv_ruleQName= ruleQName EOF
            {
             newCompositeNode(grammarAccess.getQNameRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQName=ruleQName();

            state._fsp--;

             current =iv_ruleQName.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQName"


    // $ANTLR start "ruleQName"
    // InternalXquiz.g:150:1: ruleQName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleQName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;


        	enterRule();

        try {
            // InternalXquiz.g:156:2: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) )
            // InternalXquiz.g:157:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            {
            // InternalXquiz.g:157:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            // InternalXquiz.g:158:3: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_6); 

            			current.merge(this_ID_0);
            		

            			newLeafNode(this_ID_0, grammarAccess.getQNameAccess().getIDTerminalRuleCall_0());
            		
            // InternalXquiz.g:165:3: (kw= '.' this_ID_2= RULE_ID )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==12) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalXquiz.g:166:4: kw= '.' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,12,FOLLOW_3); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getQNameAccess().getFullStopKeyword_1_0());
            	    			
            	    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_6); 

            	    				current.merge(this_ID_2);
            	    			

            	    				newLeafNode(this_ID_2, grammarAccess.getQNameAccess().getIDTerminalRuleCall_1_1());
            	    			

            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQName"


    // $ANTLR start "entryRuleAbstractQuizPart"
    // InternalXquiz.g:183:1: entryRuleAbstractQuizPart returns [EObject current=null] : iv_ruleAbstractQuizPart= ruleAbstractQuizPart EOF ;
    public final EObject entryRuleAbstractQuizPart() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAbstractQuizPart = null;


        try {
            // InternalXquiz.g:183:57: (iv_ruleAbstractQuizPart= ruleAbstractQuizPart EOF )
            // InternalXquiz.g:184:2: iv_ruleAbstractQuizPart= ruleAbstractQuizPart EOF
            {
             newCompositeNode(grammarAccess.getAbstractQuizPartRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAbstractQuizPart=ruleAbstractQuizPart();

            state._fsp--;

             current =iv_ruleAbstractQuizPart; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAbstractQuizPart"


    // $ANTLR start "ruleAbstractQuizPart"
    // InternalXquiz.g:190:1: ruleAbstractQuizPart returns [EObject current=null] : (this_QuizPart_0= ruleQuizPart | this_QuizPartRef_1= ruleQuizPartRef ) ;
    public final EObject ruleAbstractQuizPart() throws RecognitionException {
        EObject current = null;

        EObject this_QuizPart_0 = null;

        EObject this_QuizPartRef_1 = null;



        	enterRule();

        try {
            // InternalXquiz.g:196:2: ( (this_QuizPart_0= ruleQuizPart | this_QuizPartRef_1= ruleQuizPartRef ) )
            // InternalXquiz.g:197:2: (this_QuizPart_0= ruleQuizPart | this_QuizPartRef_1= ruleQuizPartRef )
            {
            // InternalXquiz.g:197:2: (this_QuizPart_0= ruleQuizPart | this_QuizPartRef_1= ruleQuizPartRef )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==13) ) {
                int LA4_1 = input.LA(2);

                if ( (LA4_1==14) ) {
                    alt4=2;
                }
                else if ( (LA4_1==RULE_ID) ) {
                    alt4=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 4, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalXquiz.g:198:3: this_QuizPart_0= ruleQuizPart
                    {

                    			newCompositeNode(grammarAccess.getAbstractQuizPartAccess().getQuizPartParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_QuizPart_0=ruleQuizPart();

                    state._fsp--;


                    			current = this_QuizPart_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalXquiz.g:207:3: this_QuizPartRef_1= ruleQuizPartRef
                    {

                    			newCompositeNode(grammarAccess.getAbstractQuizPartAccess().getQuizPartRefParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_QuizPartRef_1=ruleQuizPartRef();

                    state._fsp--;


                    			current = this_QuizPartRef_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAbstractQuizPart"


    // $ANTLR start "entryRuleQuizPartRef"
    // InternalXquiz.g:219:1: entryRuleQuizPartRef returns [EObject current=null] : iv_ruleQuizPartRef= ruleQuizPartRef EOF ;
    public final EObject entryRuleQuizPartRef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQuizPartRef = null;


        try {
            // InternalXquiz.g:219:52: (iv_ruleQuizPartRef= ruleQuizPartRef EOF )
            // InternalXquiz.g:220:2: iv_ruleQuizPartRef= ruleQuizPartRef EOF
            {
             newCompositeNode(grammarAccess.getQuizPartRefRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQuizPartRef=ruleQuizPartRef();

            state._fsp--;

             current =iv_ruleQuizPartRef; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQuizPartRef"


    // $ANTLR start "ruleQuizPartRef"
    // InternalXquiz.g:226:1: ruleQuizPartRef returns [EObject current=null] : (otherlv_0= 'part' otherlv_1= '@' ( ( ruleQName ) ) ) ;
    public final EObject ruleQuizPartRef() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalXquiz.g:232:2: ( (otherlv_0= 'part' otherlv_1= '@' ( ( ruleQName ) ) ) )
            // InternalXquiz.g:233:2: (otherlv_0= 'part' otherlv_1= '@' ( ( ruleQName ) ) )
            {
            // InternalXquiz.g:233:2: (otherlv_0= 'part' otherlv_1= '@' ( ( ruleQName ) ) )
            // InternalXquiz.g:234:3: otherlv_0= 'part' otherlv_1= '@' ( ( ruleQName ) )
            {
            otherlv_0=(Token)match(input,13,FOLLOW_7); 

            			newLeafNode(otherlv_0, grammarAccess.getQuizPartRefAccess().getPartKeyword_0());
            		
            otherlv_1=(Token)match(input,14,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getQuizPartRefAccess().getCommercialAtKeyword_1());
            		
            // InternalXquiz.g:242:3: ( ( ruleQName ) )
            // InternalXquiz.g:243:4: ( ruleQName )
            {
            // InternalXquiz.g:243:4: ( ruleQName )
            // InternalXquiz.g:244:5: ruleQName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getQuizPartRefRule());
            					}
            				

            					newCompositeNode(grammarAccess.getQuizPartRefAccess().getPartRefQuizPartCrossReference_2_0());
            				
            pushFollow(FOLLOW_2);
            ruleQName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQuizPartRef"


    // $ANTLR start "entryRuleQuizPart"
    // InternalXquiz.g:262:1: entryRuleQuizPart returns [EObject current=null] : iv_ruleQuizPart= ruleQuizPart EOF ;
    public final EObject entryRuleQuizPart() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQuizPart = null;


        try {
            // InternalXquiz.g:262:49: (iv_ruleQuizPart= ruleQuizPart EOF )
            // InternalXquiz.g:263:2: iv_ruleQuizPart= ruleQuizPart EOF
            {
             newCompositeNode(grammarAccess.getQuizPartRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQuizPart=ruleQuizPart();

            state._fsp--;

             current =iv_ruleQuizPart; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQuizPart"


    // $ANTLR start "ruleQuizPart"
    // InternalXquiz.g:269:1: ruleQuizPart returns [EObject current=null] : (otherlv_0= 'part' ( (lv_name_1_0= RULE_ID ) ) ( (lv_title_2_0= RULE_STRING ) ) ( (lv_questions_3_0= ruleAbstractQA ) )* ) ;
    public final EObject ruleQuizPart() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token lv_title_2_0=null;
        EObject lv_questions_3_0 = null;



        	enterRule();

        try {
            // InternalXquiz.g:275:2: ( (otherlv_0= 'part' ( (lv_name_1_0= RULE_ID ) ) ( (lv_title_2_0= RULE_STRING ) ) ( (lv_questions_3_0= ruleAbstractQA ) )* ) )
            // InternalXquiz.g:276:2: (otherlv_0= 'part' ( (lv_name_1_0= RULE_ID ) ) ( (lv_title_2_0= RULE_STRING ) ) ( (lv_questions_3_0= ruleAbstractQA ) )* )
            {
            // InternalXquiz.g:276:2: (otherlv_0= 'part' ( (lv_name_1_0= RULE_ID ) ) ( (lv_title_2_0= RULE_STRING ) ) ( (lv_questions_3_0= ruleAbstractQA ) )* )
            // InternalXquiz.g:277:3: otherlv_0= 'part' ( (lv_name_1_0= RULE_ID ) ) ( (lv_title_2_0= RULE_STRING ) ) ( (lv_questions_3_0= ruleAbstractQA ) )*
            {
            otherlv_0=(Token)match(input,13,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getQuizPartAccess().getPartKeyword_0());
            		
            // InternalXquiz.g:281:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalXquiz.g:282:4: (lv_name_1_0= RULE_ID )
            {
            // InternalXquiz.g:282:4: (lv_name_1_0= RULE_ID )
            // InternalXquiz.g:283:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_8); 

            					newLeafNode(lv_name_1_0, grammarAccess.getQuizPartAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getQuizPartRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalXquiz.g:299:3: ( (lv_title_2_0= RULE_STRING ) )
            // InternalXquiz.g:300:4: (lv_title_2_0= RULE_STRING )
            {
            // InternalXquiz.g:300:4: (lv_title_2_0= RULE_STRING )
            // InternalXquiz.g:301:5: lv_title_2_0= RULE_STRING
            {
            lv_title_2_0=(Token)match(input,RULE_STRING,FOLLOW_9); 

            					newLeafNode(lv_title_2_0, grammarAccess.getQuizPartAccess().getTitleSTRINGTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getQuizPartRule());
            					}
            					setWithLastConsumed(
            						current,
            						"title",
            						lv_title_2_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            // InternalXquiz.g:317:3: ( (lv_questions_3_0= ruleAbstractQA ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( ((LA5_0>=RULE_STRING && LA5_0<=RULE_ID)||LA5_0==14) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalXquiz.g:318:4: (lv_questions_3_0= ruleAbstractQA )
            	    {
            	    // InternalXquiz.g:318:4: (lv_questions_3_0= ruleAbstractQA )
            	    // InternalXquiz.g:319:5: lv_questions_3_0= ruleAbstractQA
            	    {

            	    					newCompositeNode(grammarAccess.getQuizPartAccess().getQuestionsAbstractQAParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_9);
            	    lv_questions_3_0=ruleAbstractQA();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getQuizPartRule());
            	    					}
            	    					add(
            	    						current,
            	    						"questions",
            	    						lv_questions_3_0,
            	    						"no.hal.quiz.Xquiz.AbstractQA");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQuizPart"


    // $ANTLR start "entryRuleAbstractQA"
    // InternalXquiz.g:340:1: entryRuleAbstractQA returns [EObject current=null] : iv_ruleAbstractQA= ruleAbstractQA EOF ;
    public final EObject entryRuleAbstractQA() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAbstractQA = null;


        try {
            // InternalXquiz.g:340:51: (iv_ruleAbstractQA= ruleAbstractQA EOF )
            // InternalXquiz.g:341:2: iv_ruleAbstractQA= ruleAbstractQA EOF
            {
             newCompositeNode(grammarAccess.getAbstractQARule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAbstractQA=ruleAbstractQA();

            state._fsp--;

             current =iv_ruleAbstractQA; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAbstractQA"


    // $ANTLR start "ruleAbstractQA"
    // InternalXquiz.g:347:1: ruleAbstractQA returns [EObject current=null] : (this_QA_0= ruleQA | this_QARef_1= ruleQARef ) ;
    public final EObject ruleAbstractQA() throws RecognitionException {
        EObject current = null;

        EObject this_QA_0 = null;

        EObject this_QARef_1 = null;



        	enterRule();

        try {
            // InternalXquiz.g:353:2: ( (this_QA_0= ruleQA | this_QARef_1= ruleQARef ) )
            // InternalXquiz.g:354:2: (this_QA_0= ruleQA | this_QARef_1= ruleQARef )
            {
            // InternalXquiz.g:354:2: (this_QA_0= ruleQA | this_QARef_1= ruleQARef )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( ((LA6_0>=RULE_STRING && LA6_0<=RULE_ID)) ) {
                alt6=1;
            }
            else if ( (LA6_0==14) ) {
                alt6=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // InternalXquiz.g:355:3: this_QA_0= ruleQA
                    {

                    			newCompositeNode(grammarAccess.getAbstractQAAccess().getQAParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_QA_0=ruleQA();

                    state._fsp--;


                    			current = this_QA_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalXquiz.g:364:3: this_QARef_1= ruleQARef
                    {

                    			newCompositeNode(grammarAccess.getAbstractQAAccess().getQARefParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_QARef_1=ruleQARef();

                    state._fsp--;


                    			current = this_QARef_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAbstractQA"


    // $ANTLR start "entryRuleQARef"
    // InternalXquiz.g:376:1: entryRuleQARef returns [EObject current=null] : iv_ruleQARef= ruleQARef EOF ;
    public final EObject entryRuleQARef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQARef = null;


        try {
            // InternalXquiz.g:376:46: (iv_ruleQARef= ruleQARef EOF )
            // InternalXquiz.g:377:2: iv_ruleQARef= ruleQARef EOF
            {
             newCompositeNode(grammarAccess.getQARefRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQARef=ruleQARef();

            state._fsp--;

             current =iv_ruleQARef; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQARef"


    // $ANTLR start "ruleQARef"
    // InternalXquiz.g:383:1: ruleQARef returns [EObject current=null] : (otherlv_0= '@' ( ( ruleQName ) ) ) ;
    public final EObject ruleQARef() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;


        	enterRule();

        try {
            // InternalXquiz.g:389:2: ( (otherlv_0= '@' ( ( ruleQName ) ) ) )
            // InternalXquiz.g:390:2: (otherlv_0= '@' ( ( ruleQName ) ) )
            {
            // InternalXquiz.g:390:2: (otherlv_0= '@' ( ( ruleQName ) ) )
            // InternalXquiz.g:391:3: otherlv_0= '@' ( ( ruleQName ) )
            {
            otherlv_0=(Token)match(input,14,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getQARefAccess().getCommercialAtKeyword_0());
            		
            // InternalXquiz.g:395:3: ( ( ruleQName ) )
            // InternalXquiz.g:396:4: ( ruleQName )
            {
            // InternalXquiz.g:396:4: ( ruleQName )
            // InternalXquiz.g:397:5: ruleQName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getQARefRule());
            					}
            				

            					newCompositeNode(grammarAccess.getQARefAccess().getQaRefQACrossReference_1_0());
            				
            pushFollow(FOLLOW_2);
            ruleQName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQARef"


    // $ANTLR start "entryRuleQA"
    // InternalXquiz.g:415:1: entryRuleQA returns [EObject current=null] : iv_ruleQA= ruleQA EOF ;
    public final EObject entryRuleQA() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQA = null;


        try {
            // InternalXquiz.g:415:43: (iv_ruleQA= ruleQA EOF )
            // InternalXquiz.g:416:2: iv_ruleQA= ruleQA EOF
            {
             newCompositeNode(grammarAccess.getQARule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQA=ruleQA();

            state._fsp--;

             current =iv_ruleQA; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQA"


    // $ANTLR start "ruleQA"
    // InternalXquiz.g:422:1: ruleQA returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) )? ( (lv_q_1_0= ruleQuestion ) ) (otherlv_2= '?' )? ( (lv_a_3_0= ruleAnswer ) ) ) ;
    public final EObject ruleQA() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_2=null;
        EObject lv_q_1_0 = null;

        EObject lv_a_3_0 = null;



        	enterRule();

        try {
            // InternalXquiz.g:428:2: ( ( ( (lv_name_0_0= RULE_ID ) )? ( (lv_q_1_0= ruleQuestion ) ) (otherlv_2= '?' )? ( (lv_a_3_0= ruleAnswer ) ) ) )
            // InternalXquiz.g:429:2: ( ( (lv_name_0_0= RULE_ID ) )? ( (lv_q_1_0= ruleQuestion ) ) (otherlv_2= '?' )? ( (lv_a_3_0= ruleAnswer ) ) )
            {
            // InternalXquiz.g:429:2: ( ( (lv_name_0_0= RULE_ID ) )? ( (lv_q_1_0= ruleQuestion ) ) (otherlv_2= '?' )? ( (lv_a_3_0= ruleAnswer ) ) )
            // InternalXquiz.g:430:3: ( (lv_name_0_0= RULE_ID ) )? ( (lv_q_1_0= ruleQuestion ) ) (otherlv_2= '?' )? ( (lv_a_3_0= ruleAnswer ) )
            {
            // InternalXquiz.g:430:3: ( (lv_name_0_0= RULE_ID ) )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==RULE_ID) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalXquiz.g:431:4: (lv_name_0_0= RULE_ID )
                    {
                    // InternalXquiz.g:431:4: (lv_name_0_0= RULE_ID )
                    // InternalXquiz.g:432:5: lv_name_0_0= RULE_ID
                    {
                    lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_10); 

                    					newLeafNode(lv_name_0_0, grammarAccess.getQAAccess().getNameIDTerminalRuleCall_0_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getQARule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"name",
                    						lv_name_0_0,
                    						"org.eclipse.xtext.common.Terminals.ID");
                    				

                    }


                    }
                    break;

            }

            // InternalXquiz.g:448:3: ( (lv_q_1_0= ruleQuestion ) )
            // InternalXquiz.g:449:4: (lv_q_1_0= ruleQuestion )
            {
            // InternalXquiz.g:449:4: (lv_q_1_0= ruleQuestion )
            // InternalXquiz.g:450:5: lv_q_1_0= ruleQuestion
            {

            					newCompositeNode(grammarAccess.getQAAccess().getQQuestionParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_11);
            lv_q_1_0=ruleQuestion();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getQARule());
            					}
            					set(
            						current,
            						"q",
            						lv_q_1_0,
            						"no.hal.quiz.Xquiz.Question");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalXquiz.g:467:3: (otherlv_2= '?' )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==15) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalXquiz.g:468:4: otherlv_2= '?'
                    {
                    otherlv_2=(Token)match(input,15,FOLLOW_11); 

                    				newLeafNode(otherlv_2, grammarAccess.getQAAccess().getQuestionMarkKeyword_2());
                    			

                    }
                    break;

            }

            // InternalXquiz.g:473:3: ( (lv_a_3_0= ruleAnswer ) )
            // InternalXquiz.g:474:4: (lv_a_3_0= ruleAnswer )
            {
            // InternalXquiz.g:474:4: (lv_a_3_0= ruleAnswer )
            // InternalXquiz.g:475:5: lv_a_3_0= ruleAnswer
            {

            					newCompositeNode(grammarAccess.getQAAccess().getAAnswerParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_2);
            lv_a_3_0=ruleAnswer();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getQARule());
            					}
            					set(
            						current,
            						"a",
            						lv_a_3_0,
            						"no.hal.quiz.Xquiz.Answer");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQA"


    // $ANTLR start "entryRuleQuestion"
    // InternalXquiz.g:496:1: entryRuleQuestion returns [EObject current=null] : iv_ruleQuestion= ruleQuestion EOF ;
    public final EObject entryRuleQuestion() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQuestion = null;


        try {
            // InternalXquiz.g:496:49: (iv_ruleQuestion= ruleQuestion EOF )
            // InternalXquiz.g:497:2: iv_ruleQuestion= ruleQuestion EOF
            {
             newCompositeNode(grammarAccess.getQuestionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQuestion=ruleQuestion();

            state._fsp--;

             current =iv_ruleQuestion; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQuestion"


    // $ANTLR start "ruleQuestion"
    // InternalXquiz.g:503:1: ruleQuestion returns [EObject current=null] : this_StringQuestion_0= ruleStringQuestion ;
    public final EObject ruleQuestion() throws RecognitionException {
        EObject current = null;

        EObject this_StringQuestion_0 = null;



        	enterRule();

        try {
            // InternalXquiz.g:509:2: (this_StringQuestion_0= ruleStringQuestion )
            // InternalXquiz.g:510:2: this_StringQuestion_0= ruleStringQuestion
            {

            		newCompositeNode(grammarAccess.getQuestionAccess().getStringQuestionParserRuleCall());
            	
            pushFollow(FOLLOW_2);
            this_StringQuestion_0=ruleStringQuestion();

            state._fsp--;


            		current = this_StringQuestion_0;
            		afterParserOrEnumRuleCall();
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQuestion"


    // $ANTLR start "entryRuleStringQuestion"
    // InternalXquiz.g:521:1: entryRuleStringQuestion returns [EObject current=null] : iv_ruleStringQuestion= ruleStringQuestion EOF ;
    public final EObject entryRuleStringQuestion() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStringQuestion = null;


        try {
            // InternalXquiz.g:521:55: (iv_ruleStringQuestion= ruleStringQuestion EOF )
            // InternalXquiz.g:522:2: iv_ruleStringQuestion= ruleStringQuestion EOF
            {
             newCompositeNode(grammarAccess.getStringQuestionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStringQuestion=ruleStringQuestion();

            state._fsp--;

             current =iv_ruleStringQuestion; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStringQuestion"


    // $ANTLR start "ruleStringQuestion"
    // InternalXquiz.g:528:1: ruleStringQuestion returns [EObject current=null] : ( (lv_question_0_0= RULE_STRING ) ) ;
    public final EObject ruleStringQuestion() throws RecognitionException {
        EObject current = null;

        Token lv_question_0_0=null;


        	enterRule();

        try {
            // InternalXquiz.g:534:2: ( ( (lv_question_0_0= RULE_STRING ) ) )
            // InternalXquiz.g:535:2: ( (lv_question_0_0= RULE_STRING ) )
            {
            // InternalXquiz.g:535:2: ( (lv_question_0_0= RULE_STRING ) )
            // InternalXquiz.g:536:3: (lv_question_0_0= RULE_STRING )
            {
            // InternalXquiz.g:536:3: (lv_question_0_0= RULE_STRING )
            // InternalXquiz.g:537:4: lv_question_0_0= RULE_STRING
            {
            lv_question_0_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            				newLeafNode(lv_question_0_0, grammarAccess.getStringQuestionAccess().getQuestionSTRINGTerminalRuleCall_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getStringQuestionRule());
            				}
            				setWithLastConsumed(
            					current,
            					"question",
            					lv_question_0_0,
            					"org.eclipse.xtext.common.Terminals.STRING");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStringQuestion"


    // $ANTLR start "entryRuleAnswer"
    // InternalXquiz.g:556:1: entryRuleAnswer returns [EObject current=null] : iv_ruleAnswer= ruleAnswer EOF ;
    public final EObject entryRuleAnswer() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAnswer = null;


        try {
            // InternalXquiz.g:556:47: (iv_ruleAnswer= ruleAnswer EOF )
            // InternalXquiz.g:557:2: iv_ruleAnswer= ruleAnswer EOF
            {
             newCompositeNode(grammarAccess.getAnswerRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAnswer=ruleAnswer();

            state._fsp--;

             current =iv_ruleAnswer; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAnswer"


    // $ANTLR start "ruleAnswer"
    // InternalXquiz.g:563:1: ruleAnswer returns [EObject current=null] : (this_OptionAnswer_0= ruleOptionAnswer | this_OptionsAnswer_1= ruleOptionsAnswer ) ;
    public final EObject ruleAnswer() throws RecognitionException {
        EObject current = null;

        EObject this_OptionAnswer_0 = null;

        EObject this_OptionsAnswer_1 = null;



        	enterRule();

        try {
            // InternalXquiz.g:569:2: ( (this_OptionAnswer_0= ruleOptionAnswer | this_OptionsAnswer_1= ruleOptionsAnswer ) )
            // InternalXquiz.g:570:2: (this_OptionAnswer_0= ruleOptionAnswer | this_OptionsAnswer_1= ruleOptionsAnswer )
            {
            // InternalXquiz.g:570:2: (this_OptionAnswer_0= ruleOptionAnswer | this_OptionsAnswer_1= ruleOptionsAnswer )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==RULE_STRING||LA9_0==RULE_INT||(LA9_0>=17 && LA9_0<=18)) ) {
                alt9=1;
            }
            else if ( (LA9_0==19||LA9_0==22) ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // InternalXquiz.g:571:3: this_OptionAnswer_0= ruleOptionAnswer
                    {

                    			newCompositeNode(grammarAccess.getAnswerAccess().getOptionAnswerParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_OptionAnswer_0=ruleOptionAnswer();

                    state._fsp--;


                    			current = this_OptionAnswer_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalXquiz.g:580:3: this_OptionsAnswer_1= ruleOptionsAnswer
                    {

                    			newCompositeNode(grammarAccess.getAnswerAccess().getOptionsAnswerParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_OptionsAnswer_1=ruleOptionsAnswer();

                    state._fsp--;


                    			current = this_OptionsAnswer_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAnswer"


    // $ANTLR start "entryRuleOptionAnswer"
    // InternalXquiz.g:592:1: entryRuleOptionAnswer returns [EObject current=null] : iv_ruleOptionAnswer= ruleOptionAnswer EOF ;
    public final EObject entryRuleOptionAnswer() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOptionAnswer = null;


        try {
            // InternalXquiz.g:592:53: (iv_ruleOptionAnswer= ruleOptionAnswer EOF )
            // InternalXquiz.g:593:2: iv_ruleOptionAnswer= ruleOptionAnswer EOF
            {
             newCompositeNode(grammarAccess.getOptionAnswerRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOptionAnswer=ruleOptionAnswer();

            state._fsp--;

             current =iv_ruleOptionAnswer; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOptionAnswer"


    // $ANTLR start "ruleOptionAnswer"
    // InternalXquiz.g:599:1: ruleOptionAnswer returns [EObject current=null] : this_SimpleAnswer_0= ruleSimpleAnswer ;
    public final EObject ruleOptionAnswer() throws RecognitionException {
        EObject current = null;

        EObject this_SimpleAnswer_0 = null;



        	enterRule();

        try {
            // InternalXquiz.g:605:2: (this_SimpleAnswer_0= ruleSimpleAnswer )
            // InternalXquiz.g:606:2: this_SimpleAnswer_0= ruleSimpleAnswer
            {

            		newCompositeNode(grammarAccess.getOptionAnswerAccess().getSimpleAnswerParserRuleCall());
            	
            pushFollow(FOLLOW_2);
            this_SimpleAnswer_0=ruleSimpleAnswer();

            state._fsp--;


            		current = this_SimpleAnswer_0;
            		afterParserOrEnumRuleCall();
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOptionAnswer"


    // $ANTLR start "entryRuleSimpleAnswer"
    // InternalXquiz.g:617:1: entryRuleSimpleAnswer returns [EObject current=null] : iv_ruleSimpleAnswer= ruleSimpleAnswer EOF ;
    public final EObject entryRuleSimpleAnswer() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSimpleAnswer = null;


        try {
            // InternalXquiz.g:617:53: (iv_ruleSimpleAnswer= ruleSimpleAnswer EOF )
            // InternalXquiz.g:618:2: iv_ruleSimpleAnswer= ruleSimpleAnswer EOF
            {
             newCompositeNode(grammarAccess.getSimpleAnswerRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSimpleAnswer=ruleSimpleAnswer();

            state._fsp--;

             current =iv_ruleSimpleAnswer; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSimpleAnswer"


    // $ANTLR start "ruleSimpleAnswer"
    // InternalXquiz.g:624:1: ruleSimpleAnswer returns [EObject current=null] : (this_StringAnswer_0= ruleStringAnswer | this_NumberAnswer_1= ruleNumberAnswer | this_BooleanAnswer_2= ruleBooleanAnswer ) ;
    public final EObject ruleSimpleAnswer() throws RecognitionException {
        EObject current = null;

        EObject this_StringAnswer_0 = null;

        EObject this_NumberAnswer_1 = null;

        EObject this_BooleanAnswer_2 = null;



        	enterRule();

        try {
            // InternalXquiz.g:630:2: ( (this_StringAnswer_0= ruleStringAnswer | this_NumberAnswer_1= ruleNumberAnswer | this_BooleanAnswer_2= ruleBooleanAnswer ) )
            // InternalXquiz.g:631:2: (this_StringAnswer_0= ruleStringAnswer | this_NumberAnswer_1= ruleNumberAnswer | this_BooleanAnswer_2= ruleBooleanAnswer )
            {
            // InternalXquiz.g:631:2: (this_StringAnswer_0= ruleStringAnswer | this_NumberAnswer_1= ruleNumberAnswer | this_BooleanAnswer_2= ruleBooleanAnswer )
            int alt10=3;
            switch ( input.LA(1) ) {
            case RULE_STRING:
                {
                alt10=1;
                }
                break;
            case RULE_INT:
                {
                alt10=2;
                }
                break;
            case 17:
            case 18:
                {
                alt10=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }

            switch (alt10) {
                case 1 :
                    // InternalXquiz.g:632:3: this_StringAnswer_0= ruleStringAnswer
                    {

                    			newCompositeNode(grammarAccess.getSimpleAnswerAccess().getStringAnswerParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_StringAnswer_0=ruleStringAnswer();

                    state._fsp--;


                    			current = this_StringAnswer_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalXquiz.g:641:3: this_NumberAnswer_1= ruleNumberAnswer
                    {

                    			newCompositeNode(grammarAccess.getSimpleAnswerAccess().getNumberAnswerParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_NumberAnswer_1=ruleNumberAnswer();

                    state._fsp--;


                    			current = this_NumberAnswer_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalXquiz.g:650:3: this_BooleanAnswer_2= ruleBooleanAnswer
                    {

                    			newCompositeNode(grammarAccess.getSimpleAnswerAccess().getBooleanAnswerParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_BooleanAnswer_2=ruleBooleanAnswer();

                    state._fsp--;


                    			current = this_BooleanAnswer_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSimpleAnswer"


    // $ANTLR start "entryRuleEDouble"
    // InternalXquiz.g:662:1: entryRuleEDouble returns [String current=null] : iv_ruleEDouble= ruleEDouble EOF ;
    public final String entryRuleEDouble() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEDouble = null;


        try {
            // InternalXquiz.g:662:47: (iv_ruleEDouble= ruleEDouble EOF )
            // InternalXquiz.g:663:2: iv_ruleEDouble= ruleEDouble EOF
            {
             newCompositeNode(grammarAccess.getEDoubleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEDouble=ruleEDouble();

            state._fsp--;

             current =iv_ruleEDouble.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEDouble"


    // $ANTLR start "ruleEDouble"
    // InternalXquiz.g:669:1: ruleEDouble returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_INT_0= RULE_INT (kw= '.' this_INT_2= RULE_INT )? ) ;
    public final AntlrDatatypeRuleToken ruleEDouble() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_INT_0=null;
        Token kw=null;
        Token this_INT_2=null;


        	enterRule();

        try {
            // InternalXquiz.g:675:2: ( (this_INT_0= RULE_INT (kw= '.' this_INT_2= RULE_INT )? ) )
            // InternalXquiz.g:676:2: (this_INT_0= RULE_INT (kw= '.' this_INT_2= RULE_INT )? )
            {
            // InternalXquiz.g:676:2: (this_INT_0= RULE_INT (kw= '.' this_INT_2= RULE_INT )? )
            // InternalXquiz.g:677:3: this_INT_0= RULE_INT (kw= '.' this_INT_2= RULE_INT )?
            {
            this_INT_0=(Token)match(input,RULE_INT,FOLLOW_6); 

            			current.merge(this_INT_0);
            		

            			newLeafNode(this_INT_0, grammarAccess.getEDoubleAccess().getINTTerminalRuleCall_0());
            		
            // InternalXquiz.g:684:3: (kw= '.' this_INT_2= RULE_INT )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==12) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalXquiz.g:685:4: kw= '.' this_INT_2= RULE_INT
                    {
                    kw=(Token)match(input,12,FOLLOW_12); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getEDoubleAccess().getFullStopKeyword_1_0());
                    			
                    this_INT_2=(Token)match(input,RULE_INT,FOLLOW_2); 

                    				current.merge(this_INT_2);
                    			

                    				newLeafNode(this_INT_2, grammarAccess.getEDoubleAccess().getINTTerminalRuleCall_1_1());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEDouble"


    // $ANTLR start "entryRuleStringAnswer"
    // InternalXquiz.g:702:1: entryRuleStringAnswer returns [EObject current=null] : iv_ruleStringAnswer= ruleStringAnswer EOF ;
    public final EObject entryRuleStringAnswer() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStringAnswer = null;


        try {
            // InternalXquiz.g:702:53: (iv_ruleStringAnswer= ruleStringAnswer EOF )
            // InternalXquiz.g:703:2: iv_ruleStringAnswer= ruleStringAnswer EOF
            {
             newCompositeNode(grammarAccess.getStringAnswerRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStringAnswer=ruleStringAnswer();

            state._fsp--;

             current =iv_ruleStringAnswer; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStringAnswer"


    // $ANTLR start "ruleStringAnswer"
    // InternalXquiz.g:709:1: ruleStringAnswer returns [EObject current=null] : ( ( (lv_value_0_0= RULE_STRING ) ) ( (lv_regexp_1_0= '~' ) )? ) ;
    public final EObject ruleStringAnswer() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;
        Token lv_regexp_1_0=null;


        	enterRule();

        try {
            // InternalXquiz.g:715:2: ( ( ( (lv_value_0_0= RULE_STRING ) ) ( (lv_regexp_1_0= '~' ) )? ) )
            // InternalXquiz.g:716:2: ( ( (lv_value_0_0= RULE_STRING ) ) ( (lv_regexp_1_0= '~' ) )? )
            {
            // InternalXquiz.g:716:2: ( ( (lv_value_0_0= RULE_STRING ) ) ( (lv_regexp_1_0= '~' ) )? )
            // InternalXquiz.g:717:3: ( (lv_value_0_0= RULE_STRING ) ) ( (lv_regexp_1_0= '~' ) )?
            {
            // InternalXquiz.g:717:3: ( (lv_value_0_0= RULE_STRING ) )
            // InternalXquiz.g:718:4: (lv_value_0_0= RULE_STRING )
            {
            // InternalXquiz.g:718:4: (lv_value_0_0= RULE_STRING )
            // InternalXquiz.g:719:5: lv_value_0_0= RULE_STRING
            {
            lv_value_0_0=(Token)match(input,RULE_STRING,FOLLOW_13); 

            					newLeafNode(lv_value_0_0, grammarAccess.getStringAnswerAccess().getValueSTRINGTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getStringAnswerRule());
            					}
            					setWithLastConsumed(
            						current,
            						"value",
            						lv_value_0_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            // InternalXquiz.g:735:3: ( (lv_regexp_1_0= '~' ) )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==16) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalXquiz.g:736:4: (lv_regexp_1_0= '~' )
                    {
                    // InternalXquiz.g:736:4: (lv_regexp_1_0= '~' )
                    // InternalXquiz.g:737:5: lv_regexp_1_0= '~'
                    {
                    lv_regexp_1_0=(Token)match(input,16,FOLLOW_2); 

                    					newLeafNode(lv_regexp_1_0, grammarAccess.getStringAnswerAccess().getRegexpTildeKeyword_1_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getStringAnswerRule());
                    					}
                    					setWithLastConsumed(current, "regexp", true, "~");
                    				

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStringAnswer"


    // $ANTLR start "entryRuleNumberAnswer"
    // InternalXquiz.g:753:1: entryRuleNumberAnswer returns [EObject current=null] : iv_ruleNumberAnswer= ruleNumberAnswer EOF ;
    public final EObject entryRuleNumberAnswer() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNumberAnswer = null;


        try {
            // InternalXquiz.g:753:53: (iv_ruleNumberAnswer= ruleNumberAnswer EOF )
            // InternalXquiz.g:754:2: iv_ruleNumberAnswer= ruleNumberAnswer EOF
            {
             newCompositeNode(grammarAccess.getNumberAnswerRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNumberAnswer=ruleNumberAnswer();

            state._fsp--;

             current =iv_ruleNumberAnswer; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNumberAnswer"


    // $ANTLR start "ruleNumberAnswer"
    // InternalXquiz.g:760:1: ruleNumberAnswer returns [EObject current=null] : ( (lv_value_0_0= ruleEDouble ) ) ;
    public final EObject ruleNumberAnswer() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_value_0_0 = null;



        	enterRule();

        try {
            // InternalXquiz.g:766:2: ( ( (lv_value_0_0= ruleEDouble ) ) )
            // InternalXquiz.g:767:2: ( (lv_value_0_0= ruleEDouble ) )
            {
            // InternalXquiz.g:767:2: ( (lv_value_0_0= ruleEDouble ) )
            // InternalXquiz.g:768:3: (lv_value_0_0= ruleEDouble )
            {
            // InternalXquiz.g:768:3: (lv_value_0_0= ruleEDouble )
            // InternalXquiz.g:769:4: lv_value_0_0= ruleEDouble
            {

            				newCompositeNode(grammarAccess.getNumberAnswerAccess().getValueEDoubleParserRuleCall_0());
            			
            pushFollow(FOLLOW_2);
            lv_value_0_0=ruleEDouble();

            state._fsp--;


            				if (current==null) {
            					current = createModelElementForParent(grammarAccess.getNumberAnswerRule());
            				}
            				set(
            					current,
            					"value",
            					lv_value_0_0,
            					"no.hal.quiz.Xquiz.EDouble");
            				afterParserOrEnumRuleCall();
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNumberAnswer"


    // $ANTLR start "entryRuleBooleanAnswer"
    // InternalXquiz.g:789:1: entryRuleBooleanAnswer returns [EObject current=null] : iv_ruleBooleanAnswer= ruleBooleanAnswer EOF ;
    public final EObject entryRuleBooleanAnswer() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanAnswer = null;


        try {
            // InternalXquiz.g:789:54: (iv_ruleBooleanAnswer= ruleBooleanAnswer EOF )
            // InternalXquiz.g:790:2: iv_ruleBooleanAnswer= ruleBooleanAnswer EOF
            {
             newCompositeNode(grammarAccess.getBooleanAnswerRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBooleanAnswer=ruleBooleanAnswer();

            state._fsp--;

             current =iv_ruleBooleanAnswer; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanAnswer"


    // $ANTLR start "ruleBooleanAnswer"
    // InternalXquiz.g:796:1: ruleBooleanAnswer returns [EObject current=null] : ( () ( ( (lv_value_1_0= 'yes' ) ) | otherlv_2= 'no' ) ) ;
    public final EObject ruleBooleanAnswer() throws RecognitionException {
        EObject current = null;

        Token lv_value_1_0=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalXquiz.g:802:2: ( ( () ( ( (lv_value_1_0= 'yes' ) ) | otherlv_2= 'no' ) ) )
            // InternalXquiz.g:803:2: ( () ( ( (lv_value_1_0= 'yes' ) ) | otherlv_2= 'no' ) )
            {
            // InternalXquiz.g:803:2: ( () ( ( (lv_value_1_0= 'yes' ) ) | otherlv_2= 'no' ) )
            // InternalXquiz.g:804:3: () ( ( (lv_value_1_0= 'yes' ) ) | otherlv_2= 'no' )
            {
            // InternalXquiz.g:804:3: ()
            // InternalXquiz.g:805:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getBooleanAnswerAccess().getBooleanAnswerAction_0(),
            					current);
            			

            }

            // InternalXquiz.g:811:3: ( ( (lv_value_1_0= 'yes' ) ) | otherlv_2= 'no' )
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==17) ) {
                alt13=1;
            }
            else if ( (LA13_0==18) ) {
                alt13=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }
            switch (alt13) {
                case 1 :
                    // InternalXquiz.g:812:4: ( (lv_value_1_0= 'yes' ) )
                    {
                    // InternalXquiz.g:812:4: ( (lv_value_1_0= 'yes' ) )
                    // InternalXquiz.g:813:5: (lv_value_1_0= 'yes' )
                    {
                    // InternalXquiz.g:813:5: (lv_value_1_0= 'yes' )
                    // InternalXquiz.g:814:6: lv_value_1_0= 'yes'
                    {
                    lv_value_1_0=(Token)match(input,17,FOLLOW_2); 

                    						newLeafNode(lv_value_1_0, grammarAccess.getBooleanAnswerAccess().getValueYesKeyword_1_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getBooleanAnswerRule());
                    						}
                    						setWithLastConsumed(current, "value", true, "yes");
                    					

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalXquiz.g:827:4: otherlv_2= 'no'
                    {
                    otherlv_2=(Token)match(input,18,FOLLOW_2); 

                    				newLeafNode(otherlv_2, grammarAccess.getBooleanAnswerAccess().getNoKeyword_1_1());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanAnswer"


    // $ANTLR start "entryRuleOptionsAnswer"
    // InternalXquiz.g:836:1: entryRuleOptionsAnswer returns [EObject current=null] : iv_ruleOptionsAnswer= ruleOptionsAnswer EOF ;
    public final EObject entryRuleOptionsAnswer() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOptionsAnswer = null;


        try {
            // InternalXquiz.g:836:54: (iv_ruleOptionsAnswer= ruleOptionsAnswer EOF )
            // InternalXquiz.g:837:2: iv_ruleOptionsAnswer= ruleOptionsAnswer EOF
            {
             newCompositeNode(grammarAccess.getOptionsAnswerRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOptionsAnswer=ruleOptionsAnswer();

            state._fsp--;

             current =iv_ruleOptionsAnswer; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOptionsAnswer"


    // $ANTLR start "ruleOptionsAnswer"
    // InternalXquiz.g:843:1: ruleOptionsAnswer returns [EObject current=null] : (this_SingleOptionsAnswer_0= ruleSingleOptionsAnswer | this_ManyOptionsAnswer_1= ruleManyOptionsAnswer ) ;
    public final EObject ruleOptionsAnswer() throws RecognitionException {
        EObject current = null;

        EObject this_SingleOptionsAnswer_0 = null;

        EObject this_ManyOptionsAnswer_1 = null;



        	enterRule();

        try {
            // InternalXquiz.g:849:2: ( (this_SingleOptionsAnswer_0= ruleSingleOptionsAnswer | this_ManyOptionsAnswer_1= ruleManyOptionsAnswer ) )
            // InternalXquiz.g:850:2: (this_SingleOptionsAnswer_0= ruleSingleOptionsAnswer | this_ManyOptionsAnswer_1= ruleManyOptionsAnswer )
            {
            // InternalXquiz.g:850:2: (this_SingleOptionsAnswer_0= ruleSingleOptionsAnswer | this_ManyOptionsAnswer_1= ruleManyOptionsAnswer )
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==19) ) {
                alt14=1;
            }
            else if ( (LA14_0==22) ) {
                alt14=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }
            switch (alt14) {
                case 1 :
                    // InternalXquiz.g:851:3: this_SingleOptionsAnswer_0= ruleSingleOptionsAnswer
                    {

                    			newCompositeNode(grammarAccess.getOptionsAnswerAccess().getSingleOptionsAnswerParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_SingleOptionsAnswer_0=ruleSingleOptionsAnswer();

                    state._fsp--;


                    			current = this_SingleOptionsAnswer_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalXquiz.g:860:3: this_ManyOptionsAnswer_1= ruleManyOptionsAnswer
                    {

                    			newCompositeNode(grammarAccess.getOptionsAnswerAccess().getManyOptionsAnswerParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_ManyOptionsAnswer_1=ruleManyOptionsAnswer();

                    state._fsp--;


                    			current = this_ManyOptionsAnswer_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOptionsAnswer"


    // $ANTLR start "entryRuleSingleOptionsAnswer"
    // InternalXquiz.g:872:1: entryRuleSingleOptionsAnswer returns [EObject current=null] : iv_ruleSingleOptionsAnswer= ruleSingleOptionsAnswer EOF ;
    public final EObject entryRuleSingleOptionsAnswer() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSingleOptionsAnswer = null;


        try {
            // InternalXquiz.g:872:60: (iv_ruleSingleOptionsAnswer= ruleSingleOptionsAnswer EOF )
            // InternalXquiz.g:873:2: iv_ruleSingleOptionsAnswer= ruleSingleOptionsAnswer EOF
            {
             newCompositeNode(grammarAccess.getSingleOptionsAnswerRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSingleOptionsAnswer=ruleSingleOptionsAnswer();

            state._fsp--;

             current =iv_ruleSingleOptionsAnswer; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSingleOptionsAnswer"


    // $ANTLR start "ruleSingleOptionsAnswer"
    // InternalXquiz.g:879:1: ruleSingleOptionsAnswer returns [EObject current=null] : ( (lv_options_0_0= ruleSingleListOption ) )+ ;
    public final EObject ruleSingleOptionsAnswer() throws RecognitionException {
        EObject current = null;

        EObject lv_options_0_0 = null;



        	enterRule();

        try {
            // InternalXquiz.g:885:2: ( ( (lv_options_0_0= ruleSingleListOption ) )+ )
            // InternalXquiz.g:886:2: ( (lv_options_0_0= ruleSingleListOption ) )+
            {
            // InternalXquiz.g:886:2: ( (lv_options_0_0= ruleSingleListOption ) )+
            int cnt15=0;
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==19) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalXquiz.g:887:3: (lv_options_0_0= ruleSingleListOption )
            	    {
            	    // InternalXquiz.g:887:3: (lv_options_0_0= ruleSingleListOption )
            	    // InternalXquiz.g:888:4: lv_options_0_0= ruleSingleListOption
            	    {

            	    				newCompositeNode(grammarAccess.getSingleOptionsAnswerAccess().getOptionsSingleListOptionParserRuleCall_0());
            	    			
            	    pushFollow(FOLLOW_14);
            	    lv_options_0_0=ruleSingleListOption();

            	    state._fsp--;


            	    				if (current==null) {
            	    					current = createModelElementForParent(grammarAccess.getSingleOptionsAnswerRule());
            	    				}
            	    				add(
            	    					current,
            	    					"options",
            	    					lv_options_0_0,
            	    					"no.hal.quiz.Xquiz.SingleListOption");
            	    				afterParserOrEnumRuleCall();
            	    			

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt15 >= 1 ) break loop15;
                        EarlyExitException eee =
                            new EarlyExitException(15, input);
                        throw eee;
                }
                cnt15++;
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSingleOptionsAnswer"


    // $ANTLR start "entryRuleSingleListOption"
    // InternalXquiz.g:908:1: entryRuleSingleListOption returns [EObject current=null] : iv_ruleSingleListOption= ruleSingleListOption EOF ;
    public final EObject entryRuleSingleListOption() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSingleListOption = null;


        try {
            // InternalXquiz.g:908:57: (iv_ruleSingleListOption= ruleSingleListOption EOF )
            // InternalXquiz.g:909:2: iv_ruleSingleListOption= ruleSingleListOption EOF
            {
             newCompositeNode(grammarAccess.getSingleListOptionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSingleListOption=ruleSingleListOption();

            state._fsp--;

             current =iv_ruleSingleListOption; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSingleListOption"


    // $ANTLR start "ruleSingleListOption"
    // InternalXquiz.g:915:1: ruleSingleListOption returns [EObject current=null] : (otherlv_0= '(' ( (lv_correct_1_0= 'x' ) )? otherlv_2= ')' ( (lv_option_3_0= ruleOptionAnswer ) ) ) ;
    public final EObject ruleSingleListOption() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_correct_1_0=null;
        Token otherlv_2=null;
        EObject lv_option_3_0 = null;



        	enterRule();

        try {
            // InternalXquiz.g:921:2: ( (otherlv_0= '(' ( (lv_correct_1_0= 'x' ) )? otherlv_2= ')' ( (lv_option_3_0= ruleOptionAnswer ) ) ) )
            // InternalXquiz.g:922:2: (otherlv_0= '(' ( (lv_correct_1_0= 'x' ) )? otherlv_2= ')' ( (lv_option_3_0= ruleOptionAnswer ) ) )
            {
            // InternalXquiz.g:922:2: (otherlv_0= '(' ( (lv_correct_1_0= 'x' ) )? otherlv_2= ')' ( (lv_option_3_0= ruleOptionAnswer ) ) )
            // InternalXquiz.g:923:3: otherlv_0= '(' ( (lv_correct_1_0= 'x' ) )? otherlv_2= ')' ( (lv_option_3_0= ruleOptionAnswer ) )
            {
            otherlv_0=(Token)match(input,19,FOLLOW_15); 

            			newLeafNode(otherlv_0, grammarAccess.getSingleListOptionAccess().getLeftParenthesisKeyword_0());
            		
            // InternalXquiz.g:927:3: ( (lv_correct_1_0= 'x' ) )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==20) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalXquiz.g:928:4: (lv_correct_1_0= 'x' )
                    {
                    // InternalXquiz.g:928:4: (lv_correct_1_0= 'x' )
                    // InternalXquiz.g:929:5: lv_correct_1_0= 'x'
                    {
                    lv_correct_1_0=(Token)match(input,20,FOLLOW_16); 

                    					newLeafNode(lv_correct_1_0, grammarAccess.getSingleListOptionAccess().getCorrectXKeyword_1_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getSingleListOptionRule());
                    					}
                    					setWithLastConsumed(current, "correct", true, "x");
                    				

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,21,FOLLOW_17); 

            			newLeafNode(otherlv_2, grammarAccess.getSingleListOptionAccess().getRightParenthesisKeyword_2());
            		
            // InternalXquiz.g:945:3: ( (lv_option_3_0= ruleOptionAnswer ) )
            // InternalXquiz.g:946:4: (lv_option_3_0= ruleOptionAnswer )
            {
            // InternalXquiz.g:946:4: (lv_option_3_0= ruleOptionAnswer )
            // InternalXquiz.g:947:5: lv_option_3_0= ruleOptionAnswer
            {

            					newCompositeNode(grammarAccess.getSingleListOptionAccess().getOptionOptionAnswerParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_2);
            lv_option_3_0=ruleOptionAnswer();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSingleListOptionRule());
            					}
            					set(
            						current,
            						"option",
            						lv_option_3_0,
            						"no.hal.quiz.Xquiz.OptionAnswer");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSingleListOption"


    // $ANTLR start "entryRuleManyOptionsAnswer"
    // InternalXquiz.g:968:1: entryRuleManyOptionsAnswer returns [EObject current=null] : iv_ruleManyOptionsAnswer= ruleManyOptionsAnswer EOF ;
    public final EObject entryRuleManyOptionsAnswer() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleManyOptionsAnswer = null;


        try {
            // InternalXquiz.g:968:58: (iv_ruleManyOptionsAnswer= ruleManyOptionsAnswer EOF )
            // InternalXquiz.g:969:2: iv_ruleManyOptionsAnswer= ruleManyOptionsAnswer EOF
            {
             newCompositeNode(grammarAccess.getManyOptionsAnswerRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleManyOptionsAnswer=ruleManyOptionsAnswer();

            state._fsp--;

             current =iv_ruleManyOptionsAnswer; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleManyOptionsAnswer"


    // $ANTLR start "ruleManyOptionsAnswer"
    // InternalXquiz.g:975:1: ruleManyOptionsAnswer returns [EObject current=null] : ( (lv_options_0_0= ruleManyOption ) )+ ;
    public final EObject ruleManyOptionsAnswer() throws RecognitionException {
        EObject current = null;

        EObject lv_options_0_0 = null;



        	enterRule();

        try {
            // InternalXquiz.g:981:2: ( ( (lv_options_0_0= ruleManyOption ) )+ )
            // InternalXquiz.g:982:2: ( (lv_options_0_0= ruleManyOption ) )+
            {
            // InternalXquiz.g:982:2: ( (lv_options_0_0= ruleManyOption ) )+
            int cnt17=0;
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( (LA17_0==22) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // InternalXquiz.g:983:3: (lv_options_0_0= ruleManyOption )
            	    {
            	    // InternalXquiz.g:983:3: (lv_options_0_0= ruleManyOption )
            	    // InternalXquiz.g:984:4: lv_options_0_0= ruleManyOption
            	    {

            	    				newCompositeNode(grammarAccess.getManyOptionsAnswerAccess().getOptionsManyOptionParserRuleCall_0());
            	    			
            	    pushFollow(FOLLOW_18);
            	    lv_options_0_0=ruleManyOption();

            	    state._fsp--;


            	    				if (current==null) {
            	    					current = createModelElementForParent(grammarAccess.getManyOptionsAnswerRule());
            	    				}
            	    				add(
            	    					current,
            	    					"options",
            	    					lv_options_0_0,
            	    					"no.hal.quiz.Xquiz.ManyOption");
            	    				afterParserOrEnumRuleCall();
            	    			

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt17 >= 1 ) break loop17;
                        EarlyExitException eee =
                            new EarlyExitException(17, input);
                        throw eee;
                }
                cnt17++;
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleManyOptionsAnswer"


    // $ANTLR start "entryRuleManyOption"
    // InternalXquiz.g:1004:1: entryRuleManyOption returns [EObject current=null] : iv_ruleManyOption= ruleManyOption EOF ;
    public final EObject entryRuleManyOption() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleManyOption = null;


        try {
            // InternalXquiz.g:1004:51: (iv_ruleManyOption= ruleManyOption EOF )
            // InternalXquiz.g:1005:2: iv_ruleManyOption= ruleManyOption EOF
            {
             newCompositeNode(grammarAccess.getManyOptionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleManyOption=ruleManyOption();

            state._fsp--;

             current =iv_ruleManyOption; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleManyOption"


    // $ANTLR start "ruleManyOption"
    // InternalXquiz.g:1011:1: ruleManyOption returns [EObject current=null] : (otherlv_0= '[' ( (lv_correct_1_0= 'x' ) )? otherlv_2= ']' ( (lv_option_3_0= ruleOptionAnswer ) ) ) ;
    public final EObject ruleManyOption() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_correct_1_0=null;
        Token otherlv_2=null;
        EObject lv_option_3_0 = null;



        	enterRule();

        try {
            // InternalXquiz.g:1017:2: ( (otherlv_0= '[' ( (lv_correct_1_0= 'x' ) )? otherlv_2= ']' ( (lv_option_3_0= ruleOptionAnswer ) ) ) )
            // InternalXquiz.g:1018:2: (otherlv_0= '[' ( (lv_correct_1_0= 'x' ) )? otherlv_2= ']' ( (lv_option_3_0= ruleOptionAnswer ) ) )
            {
            // InternalXquiz.g:1018:2: (otherlv_0= '[' ( (lv_correct_1_0= 'x' ) )? otherlv_2= ']' ( (lv_option_3_0= ruleOptionAnswer ) ) )
            // InternalXquiz.g:1019:3: otherlv_0= '[' ( (lv_correct_1_0= 'x' ) )? otherlv_2= ']' ( (lv_option_3_0= ruleOptionAnswer ) )
            {
            otherlv_0=(Token)match(input,22,FOLLOW_19); 

            			newLeafNode(otherlv_0, grammarAccess.getManyOptionAccess().getLeftSquareBracketKeyword_0());
            		
            // InternalXquiz.g:1023:3: ( (lv_correct_1_0= 'x' ) )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==20) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalXquiz.g:1024:4: (lv_correct_1_0= 'x' )
                    {
                    // InternalXquiz.g:1024:4: (lv_correct_1_0= 'x' )
                    // InternalXquiz.g:1025:5: lv_correct_1_0= 'x'
                    {
                    lv_correct_1_0=(Token)match(input,20,FOLLOW_20); 

                    					newLeafNode(lv_correct_1_0, grammarAccess.getManyOptionAccess().getCorrectXKeyword_1_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getManyOptionRule());
                    					}
                    					setWithLastConsumed(current, "correct", true, "x");
                    				

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,23,FOLLOW_17); 

            			newLeafNode(otherlv_2, grammarAccess.getManyOptionAccess().getRightSquareBracketKeyword_2());
            		
            // InternalXquiz.g:1041:3: ( (lv_option_3_0= ruleOptionAnswer ) )
            // InternalXquiz.g:1042:4: (lv_option_3_0= ruleOptionAnswer )
            {
            // InternalXquiz.g:1042:4: (lv_option_3_0= ruleOptionAnswer )
            // InternalXquiz.g:1043:5: lv_option_3_0= ruleOptionAnswer
            {

            					newCompositeNode(grammarAccess.getManyOptionAccess().getOptionOptionAnswerParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_2);
            lv_option_3_0=ruleOptionAnswer();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getManyOptionRule());
            					}
            					set(
            						current,
            						"option",
            						lv_option_3_0,
            						"no.hal.quiz.Xquiz.OptionAnswer");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleManyOption"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000002012L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000002002L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000001002L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000004032L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x00000000004E8050L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000300000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000060050L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x00000000004E8052L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000900000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000800000L});

}