# XHTML model

An Ecore model project for the XHTML1 standard.

## XHTML 
XHTML is an attempt at defining HTML as an pure XML language. Version 1 is fairly simple and old.

## Purpose

With an Ecore model of (X)HTML, we can use it as the target of an M2M transformation, and hence, generate web contents from model instances. 

## The model

The ecore and genmodel has been generated from the XHTML XML Schema using the standard XSD importer.
The HTML classes have been renamed by removing the Type suffix you get by default (IIRC).
By using an XML Resource and generated XML annotations, instances of the model serializes as proper HTML (with minimal post-processing).

There's essentially one class pr. element kind and containment references are used for contents.
